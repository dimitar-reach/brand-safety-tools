module.exports = {
  verbose: true,
  collectCoverageFrom: [
    'server/**/*.js',
    '**/utilities/**/*.js',
    '**/services/**/*.js',
    '!**/node_modules/**',
    '!**/vendor/**',
    '!server/**/*.mock.js',
    '!**/utilities/**/*.mock.js',
    '!**/services/**/*.mock.js'
  ],
  modulePathIgnorePatterns: ['<rootDir>/client/public/assets']
};
