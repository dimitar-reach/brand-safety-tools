# Generic environment variables

# create a CSV log file with a line for each analyzed article, containing the rating of the article
write_to_log = false

# in the CSV log file, add also the rating of each rule. Applies only if write_to_log is true
write_rules_to_log = false

# store result JSONs as files on server used as cache in future requests
write_to_cache = false

# disable analyzing images identified in HTML articles 
analyze_images = true

# NLU credentials
# REACH LITE
# natural_language_understanding_apikey = QtsNjsGnH7EDVpGnt1pMYPJWpXSBblUOlcuUcHBRoZKe
# STANDARD
natural_language_understanding_apikey = XdI5A2rpoep08K_Kifk79ufZ19pe_HJcRoY3GMtxoZr-
natural_language_understanding_url = https://api.eu-gb.natural-language-understanding.watson.cloud.ibm.com/instances/2466706b-d8cf-44c2-aa09-8df5f5f4673b
natural_language_understanding_version = 2020-08-01

# VR credentials
# REACH
visual_recognition_apikey = 4bJgxvWJ1Q2ESI-gfWca5piXYhKJWRaONk8J4Ua3eipL
visual_recognition_url = https://gateway.watsonplatform.net/visual-recognition/api
visual_recognition_version = 2019-02-01

# PSQL credentials
# Standard Plan
write_to_db = false
read_from_db_cache = true
# STANDARD
postgreSQL_connectionString = postgres://admin:adm1nPassw0rd@96d59a06-d5f1-4a94-9051-a35bd5a22ee0.bm49h38l0bihsg5ne8d0.databases.appdomain.cloud:31800/ibmclouddb?sslmode=verify-full
postgreSQL_certificate_base64 = -----BEGIN CERTIFICATE-----
MIIDHTCCAgWgAwIBAgIUAkNMzbJeAz2OfRxfoSF4TpLcA6owDQYJKoZIhvcNAQEL
BQAwHjEcMBoGA1UEAwwTSUJNIENsb3VkIERhdGFiYXNlczAeFw0xODExMjExMTQ3
MjVaFw0yODExMTgxMTQ3MjVaMB4xHDAaBgNVBAMME0lCTSBDbG91ZCBEYXRhYmFz
ZXMwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCvDiYeqX19sbLdfIyY
rR3mjtcTJirYkmYmhJUYujuRSvz4Qu+lu3wwzEgX1jTuUo6difb0CFleJX1fLTvk
lfUKhUREHdaJOirtB+445XQg/W86vEx4m0UpURXQMaNVKqPUEclBZvI9i7m39/Qx
e8CNvgjKx+zadPtNzElEMpPmCsi5q8FTKV9A4L9c5VvZ6k03pnzyNnPy4WeXAoN7
LUhsLX/GAfQQDkgpb6dtv7Q9pLJOnGXQXpra8hTabgqWLmOv/1iWX/wn5cbGU0uM
x7OfvqWzkJP6qkg7MdiLXSEcrAGeJE49/sxsacWziwgpOWayKOSdkxvawfXXLKkB
km2NAgMBAAGjUzBRMB0GA1UdDgQWBBRmmBhR/78p6bTi1n9orMteUkoDOjAfBgNV
HSMEGDAWgBRmmBhR/78p6bTi1n9orMteUkoDOjAPBgNVHRMBAf8EBTADAQH/MA0G
CSqGSIb3DQEBCwUAA4IBAQBo5mJwhba611wyBM2/gd1801Ace+E/QjdfWJQuUUI8
I2FGLCkBxq99HXnpokI0XyK3uSG5MYCyx8kjcnqd58oPp+If4CwNLEB/fJFqlIDt
LBFoegAT6n7B6IwHehe4fwP/Rmkd1O53lJ5E8WYXlQ6oqKQxe4J9DWt9bE7LWgVg
aLKni/LhybTsTlDHANZ3G89M7ZQw1VEJ4NAgVSbsjCUEJSMBWX/FS383R14XIDa2
lqTCiie89peszqIhCoJQUtBP9oQpcOmTCCaDQ9fkEa122g3VLY7sTwqGG5zrGGGN
5OdAjKnMQPDNXnaRFFFgsLDAYT8DVoma9AxgkMtD2rja
-----END CERTIFICATE-----
