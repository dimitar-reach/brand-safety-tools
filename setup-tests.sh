echo '================='
rm .npmrc
echo "//registry.npmjs.org/:_authToken=${NPM_TOKEN}" > .npmrc
npm install
echo 'Copying ================='
cp -R node_modules/@trinitymirrordigital/mantis-analyzer-service/. services/analyzer
cp -R node_modules/@trinitymirrordigital/mantis-configuration-service/. services/configuration
cp -R node_modules/@trinitymirrordigital/mantis-rule-management-service/. services/rule-management
cp -R node_modules/@trinitymirrordigital/mantis-authentication-utility/. utilities/authentication
cp -R node_modules/@trinitymirrordigital/mantis-log-utility/. utilities/log
cp -R node_modules/@trinitymirrordigital/mantis-object-utility/. utilities/object
cp -R node_modules/@trinitymirrordigital/mantis-proxies-utility/. utilities/proxies

npm test