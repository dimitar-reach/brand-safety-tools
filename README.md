# Start

- get an npm token by logging with the commonly used npm user/pass (ask Ian if you don't have it)
- nano ~/.bash_aliases or nano ~/.bash_profile
- add export `NPM_TOKEN="XXXXX-XXXXX-XXXXX-XXXXX"`
- run `git submodule update --init`
- run the `./startall.sh` script on the cli

# Deploy
- When you made your changes and wish to deploy, and there is a toolchain available, run npm install then commit the package.json and package-lock.json files
- You can then test your code on bertha or deploy to stable

# Other
REACH ARTICLE AD ANALYSER 
Start server app with 
	node app.js
	
Start client with
	http://localhost:6003 (see server log for port number)
	login with basic authentication: reach/watson
	
Define userIDs and passwords in ./server/resources/users.json

Connection details for the .env file you will need to create:
# create a CSV log file with a line for each analyzed article, containing the rating of the article
write_to_log = false

# in the CSV log file, add also the rating of each rule. Applies only if write_to_log is true
write_rules_to_log = false

# store result JSONs as files on server used as cache in future requests
write_to_cache = false

# disable analyzing images identified in HTML articles 
analyze_images = false

# NLU credentials
# REACH
natural_language_understanding_apikey = QtsNjsGnH7EDVpGnt1pMYPJWpXSBblUOlcuUcHBRoZKe
natural_language_understanding_url = https://gateway-lon.watsonplatform.net/natural-language-understanding/api
natural_language_understanding_version = 2019-02-01


# VR credentials
# REACH
visual_recognition_apikey = 4bJgxvWJ1Q2ESI-gfWca5piXYhKJWRaONk8J4Ua3eipL
visual_recognition_url = https://gateway.watsonplatform.net/visual-recognition/api
visual_recognition_version = 2019-02-01


# WDS credentials. Set write_to_wds = true to enable writing to WDS.
# REACH
write_to_wds = false
discovery_url = https://gateway-lon.watsonplatform.net/discovery/api
discovery_apikey = C8_1zp203Dzwe3nbPWcXh0e8bl3LeN37r7cDsED-IqVz
discovery_version = 2019-02-01
discovery_collectionid = 
discovery_environmentid = 

