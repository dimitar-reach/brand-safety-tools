/* eslint-disable prefer-const */
async function get(url, creds) {
  if (!creds) return;
  const basicAuth = btoa(`${creds.username}:${creds.password}`);

  try {
    const res = await fetch(url, {
      // headers: {
      //     'Authorization': `Basic ${basicAuth}`,
      // },
    });
    return await res.json();
  } catch (err) {
    console.log(err.message);
  }
}

const apiServer = {
  //	    baseUrl: 'https://reach-server.mybluemix.net',
  //	    baseUrl: 'http://localhost:6002',
  baseUrl: '',

  async findAllDocsMeta(creds) {
    if (!creds) return;
    const res = await get(`${this.baseUrl}/documents`, creds);
    if (!res.results) return;
    const allDocs = res.results.map(doc => {
      return {
        id: doc.id,
        filename: doc.extracted_metadata.filename,
        author: doc.input.author,
        category: doc.input.category,
        topic: doc.input.topic,
        title: doc.title,
        rating: doc.rating.rating
      };
    });
    return allDocs;
  },

  async classifyArticle(url, creds, path) {
    if (!url || !creds) return;
    path = path || 'classifyArticle';
    const fullRatings = path === 'section/scan' ? 'ratings,' : 'fullRatings,';
    return await get(`${this.baseUrl}/${path}?filter=${fullRatings}input,findings,sentiment,emotion,categories&url=${url}`, creds);
  },

  async findDocById(id, creds) {
    if (!id || !creds) return;
    return await get(`${this.baseUrl}/documents/${id}`, creds);
  },

  async findDocByFilename(filename, creds) {
    if (!filename) return;
    const allDocs = await this.findAllDocsMeta(creds);
    if (!allDocs) return;
    const doc = allDocs.find(doc => doc.filename.toLowerCase().includes(filename.toLowerCase()));
    if (!doc) return;
    return await this.findDocById(doc.id, creds);
  },

  async uploadRules(file, creds) {
    if (!creds) return 'Error: no credentials provided';

    const basicAuth = btoa(`${creds.username}:${creds.password}`);

    try {
      let data = new FormData();
      data.append('file', file);

      const res = await fetch('/ruleset/ratings/', {
        method: 'POST',
        body: data
      });
      return await res.json();
    } catch (err) {
      console.log(err.message);
      return err;
    }
  },
  async uploadMarketplaceMapping(file, creds) {
    if (!creds) return 'Error: no credentials provided';

    const basicAuth = btoa(`${creds.username}:${creds.password}`);

    try {
      let data = new FormData();
      data.append('file', file);

      const res = await fetch('/marketplaceMapping/file/', {
        method: 'POST',
        body: data
      });
      return await res.json();
    } catch (err) {
      console.log(err.message);
      return err;
    }
  },
  async sendFeedback(article, creds) {
    if (!creds) return 'Error: no credentials provided';

    const basicAuth = btoa(`${creds.username}:${creds.password}`);

    try {
      const res = await fetch('/feedback', {
        method: 'POST',
        body: JSON.stringify(article),
        headers: {
          'Content-Type': 'application/json'
        }
      });
      return await res.json();
    } catch (err) {
      console.log(err.message);
      return err;
    }
  },

  async loadStats(creds) {
    if (!creds) return 'Error: no credentials provided';

    const basicAuth = btoa(`${creds.username}:${creds.password}`);

    try {
      const res = await fetch('/getRatingsConfusionMatrix', {
        method: 'GET'
      });
      return await res.json();
    } catch (err) {
      console.log(err.message);
      return err;
    }
  },
  async getRatingsRulesets(count, creds) {
    if (!creds) return 'Error: no credentials provided';

    let path = '/ruleset/ratings';
    if (count && count > 0) {
      path += `?count=${count}`;
    }
    try {
      const res = await fetch(path, {
        method: 'GET'
      });
      const json = await res.json();
      if (res.status !== 200) {
        return { error: json };
      }
      return json;
    } catch (err) {
      console.log(err.message);
      return err;
    }
  },
  async getMarketplaceMappingFiles(count, creds) {
    if (!creds) return 'Error: no credentials provided';

    let path = '/marketplaceMapping/files/';
    if (count && count > 0) {
      path += `?count=${count}`;
    }
    try {
      const res = await fetch(path, {
        method: 'GET'
      });
      const json = await res.json();
      if (res.status !== 200) {
        return { error: json };
      }
      return json;
    } catch (err) {
      console.log(err.message);
      return err;
    }
  },

  async deleteMarketplaceMappingFile(version, creds) {
    if (!creds) return 'Error: no credentials provided';

    let path = `/marketplaceMapping/delete/file/version/${version}`;
    try {
      const res = await fetch(path, {
        method: 'POST'
      });
      const json = await res.json();
      if (res.status !== 200) {
        return { error: json };
      }
      return json;
    } catch (err) {
      console.log(err.message);
      return err;
    }
  },

  async startRatingsRecalculation(options, creds) {
    if (!creds) return 'Error: no credentials provided';

    const basicAuth = btoa(`${creds.username}:${creds.password}`);

    let path = '/ratings-recalculation/start';
    let headers = { 'Content-Type': 'application/json' };
    if (window.location.host.indexOf('localhost:') !== -1) {
      path = 'http://localhost:6005/ratings-recalculation/start';
      headers.Authorization = `Basic ${basicAuth}`;
    }

    try {
      const res = await fetch(path, {
        method: 'POST',
        body: JSON.stringify(options),
        headers
      });
      const json = await res.json();
      if (res.status !== 200) {
        return { error: json };
      }
      return json;
    } catch (err) {
      console.log(err.message);
      return { error: err };
    }
  },
  async stopRatingsRecalculation(creds) {
    if (!creds) return 'Error: no credentials provided';

    const basicAuth = btoa(`${creds.username}:${creds.password}`);

    let path = '/ratings-recalculation/stop';
    let headers = { 'Content-Type': 'application/json' };
    if (window.location.host.indexOf('localhost:') !== -1) {
      path = 'http://localhost:6005/ratings-recalculation/stop';
      headers.Authorization = `Basic ${basicAuth}`;
    }

    try {
      const res = await fetch(path, {
        method: 'POST',
        headers
      });
      const json = await res.json();
      if (res.status !== 200) {
        return { error: json };
      }
      return json;
    } catch (err) {
      console.log(err.message);
      return { error: err };
    }
  },
  async getLatestRecalculationStatus(creds) {
    if (!creds) return 'Error: no credentials provided';

    const basicAuth = btoa(`${creds.username}:${creds.password}`);

    let path = '/ratings-recalculation/status/latest';
    let headers = {};
    if (window.location.host.indexOf('localhost:') !== -1) {
      path = 'http://localhost:6005/ratings-recalculation/status/latest';
      headers.Authorization = `Basic ${basicAuth}`;
    }
    try {
      const res = await fetch(path, { method: 'GET', headers });
      const json = await res.json();
      if (res.status !== 200) {
        return { error: json };
      }
      return json;
    } catch (err) {
      console.log(err.message);
      return { error: err };
    }
  },
  async getAllRecalculationStatus(creds) {
    if (!creds) return 'Error: no credentials provided';

    const basicAuth = btoa(`${creds.username}:${creds.password}`);

    let path = '/ratings-recalculation/status';
    let headers = {};
    if (window.location.host.indexOf('localhost:') !== -1) {
      path = 'http://localhost:6005/ratings-recalculation/status';
      headers.Authorization = `Basic ${basicAuth}`;
    }
    try {
      const res = await fetch(path, { method: 'GET', headers });
      const json = await res.json();
      if (res.status !== 200) {
        return { error: json };
      }
      return json;
    } catch (err) {
      console.log(err.message);
      return { error: err };
    }
  }
};

function parseCookies() {
  const stringCookies = document.cookie.replace(/\s/g, '').split(';');
  return stringCookies.reduce((result, cookie) => {
    const index = cookie.indexOf('=');
    if (index !== -1) {
      result[cookie.slice(0, index)] = cookie.slice(index + 1);
    }
    return result;
  }, {});
}

function getFeatureList() {
  const featuresStr = (parseCookies() || {}).clfeat || {};
  if (!featuresStr || featuresStr.length === 0) {
    return {};
  }
  try {
    return JSON.parse(atob(featuresStr));
  } catch (e) {
    console.log(e);
  }
  return {};
}
