const vm = new Vue({
  el: '#app',
  filters: {},
  data: {
    isLoading: true,

    stats: [],
    customers: [],

    creds: {
      username: 'reach',
      password: 'watson'
    }
  },
  computed: {},
  async mounted() {
    await this.loadStats();
  },
  watch: {},
  methods: {
    async loadStats() {
      this.isLoading = true;
      this.stats = await apiServer.loadStats(this.creds);
      console.log(this.stats);
      this.extractCustomers();
      this.isLoading = false;
      return;
    },

    count(customer, calculatedRating, expectedRating) {
      for (let i = 0; i < this.stats.length; i++) {
        if (this.stats[i].customer == customer && this.stats[i].calculated_rating == calculatedRating && this.stats[i].expected_rating == expectedRating) {
          return this.stats[i].count;
        }
      }
      return 0;
    },

    totalCalculated(customer, calculatedRating) {
      let total = 0;
      for (let i = 0; i < this.stats.length; i++) {
        if (this.stats[i].customer == customer && this.stats[i].calculated_rating == calculatedRating && this.stats[i].expected_rating) {
          total += parseInt(this.stats[i].count);
        }
      }
      return total;
    },

    totalExpected(customer, expectedRating) {
      let total = 0;
      for (let i = 0; i < this.stats.length; i++) {
        if (this.stats[i].customer == customer && this.stats[i].calculated_rating && this.stats[i].expected_rating == expectedRating) {
          total += parseInt(this.stats[i].count);
        }
      }
      return total;
    },

    getMatchPct(customer) {
      const count = this.getCount(customer);
      if (count == 0) {
        return 0;
      }
      return Math.round((100 * this.getMatchCount(customer)) / count);
    },

    getMatchCount(customer) {
      let total = 0;
      for (let i = 0; i < this.stats.length; i++) {
        if (this.stats[i].customer == customer && this.stats[i].calculated_rating == this.stats[i].expected_rating) {
          total += parseInt(this.stats[i].count);
        }
      }
      return total;
    },

    getCount(customer) {
      let total = 0;
      for (let i = 0; i < this.stats.length; i++) {
        if (this.stats[i].customer == customer && this.stats[i].expected_rating) {
          total += parseInt(this.stats[i].count);
        }
      }
      return total;
    },

    extractCustomers() {
      this.customers = ['Default'];
      for (let i = 0; i < this.stats.length; i++) {
        if (!this.customers.includes(this.stats[i].customer)) {
          this.customers.push(this.stats[i].customer);
        }
      }
    },

    getCustomers() {
      return this.customers;
    },
    getCalculatedRatings() {
      return ['RED', 'AMBER', 'GREEN', 'N/A'];
    },
    getExpectedRatings() {
      return ['RED', 'AMBER', 'GREEN', 'N/A', null];
    }
  }
});
