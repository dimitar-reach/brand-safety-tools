const vm = new Vue({
  el: '#app',
  filters: {
    minutes(value) {
      if (!value) return '- mins';
      return `${value}mins`;
    },
    category(value) {
      if (!value) return 'Category';
      let words = value.split('_');
      words = words.map(word => _.capitalize(word));
      return words.join(' ');
    }
  },
  data: {
    isLoading: true,
    fileMeta: {},
    article: {},
    currentCustomerRating: {},
    currentCustomer: 'Default',
    ratingFilters: ['RED', 'AMBER'],
    isAskNewUrlDocModalShowing: false,
    askNewUrlDoc: '',
    isAskFeedbackModalShowing: false,

    creds: {
      username: 'reach',
      password: 'watson'
    }
  },
  computed: {},
  async mounted() {
    if (document.cookie.indexOf('configuration_service=true') !== -1) {
      window.location.href = '/admin.html';
    } else {
      this.getFileMeta();
      await this.loadArticle();
    }
    //        this.sortImages();
  },
  watch: {},
  methods: {
    getFileMeta() {
      const search = new URL(window.location).searchParams;
      this.fileMeta = {
        filename: search.get('filename'),
        date: search.get('date') || moment().format('DD/MM/YYYY'),
        category: search.get('category') || 'Category',
        topic: search.get('topic') || 'Topic',
        author: search.get('author') || 'Author',
        title: search.get('title') || 'Title',
        id: search.get('id') || 'Id',
        url: this.normalizeUrl(search.get('url') || this.fileMeta.url),
        admin: search.get('admin') || false
      };
    },

    async loadArticle() {
      // First try the given filename
      //            this.article = await apiServer.findDocByFilename(this.fileMeta.filename, this.creds);
      //            this.article = await apiServer.findDocById(this.fileMeta.id, this.creds);
      this.isLoading = true;
      if (this.fileMeta.url) {
        const isSectionScan = document.querySelector('#isSectionScan').checked;
        const path = isSectionScan ? 'section/scan' : 'classifyArticle';
        this.article = await apiServer.classifyArticle(isSectionScan ? 'https://' + this.fileMeta.url : this.fileMeta.url, this.creds, path);
        this.article.ratings = isSectionScan ? this.article.ratings : this.article.fullRatings;
        if (this.article) {
          this.isLoading = false;
          console.log(this.article);
          this.setCurrentCustomer(this.currentCustomer);
          this.hideAskNewUrl();
          this.hideAskFeedback();
          this.$nextTick(function() {
            // Code that will run only after the
            // entire view has been re-rendered
            this.updateFeedbackIcons(false);
          });

          return;
        }
      }
      this.isLoading = false;
      this.askNewUrl();
      return;
    },

    setCurrentCustomer(currentCustomer) {
      let nextCustomer = '';
      let nextCustomerRating = {};

      if (this.article.ratings && this.article.ratings.length > 0) {
        nextCustomerRating = this.article.ratings[0];

        for (let i = 1; i < this.article.ratings.length; i++) {
          if (this.article.ratings[i].customer == currentCustomer) {
            nextCustomerRating = this.article.ratings[i];
            break;
          }
        }
        nextCustomer = nextCustomerRating.customer;
      }
      this.currentCustomerRating = nextCustomerRating;
      this.currentCustomer = nextCustomer;
    },

    sortImages() {
      if (this.article && this.article.rating && this.article.images) {
        this.article.rating.images.sort((a, b) => {
          const aVal = this.colourToValue(a.rating);
          const bVal = this.colourToValue(b.rating);

          if (aVal < bVal) return -1;
          if (aVal > bVal) return 1;
          return 0;
        });
      }
    },

    ratingToClass(rating) {
      switch (rating) {
        case 'RED':
          return 'color-red';
        case 'AMBER':
          return 'color-amber';
        case 'GREEN':
          return 'color-green';
        case 'N/A':
          return 'color-unknown';
        default:
          return 'color-unknown';
      }
    },
    getEmotionKeys(article) {
      return Object.keys(article.emotion);
    },
    getRatingCount(customerRating, colour) {
      let count = 0;
      ((customerRating.text || {}).rules || []).forEach(rule => {
        if (rule.rating === colour) ++count;
      });
      customerRating.images.forEach(image => {
        image.rules.forEach(rule => {
          if (rule.rating === colour) ++count;
        });
      });
      return count;
    },

    filterRatings(colour) {
      // If this filter button clicked is currently selected
      // Deselect current filter
      if (this.ratingFilters.includes(colour)) {
        this.ratingFilters = this.ratingFilters.filter(item => item != colour);
      } else this.ratingFilters.push(colour);
    },

    filteredRulesCount() {
      let count = this.filteredTextRules().length;
      const images = this.filteredImagesRules();
      for (let i = 0; i < images.length; i++) {
        count += images[i].rules.length;
      }
      return count;
    },

    filteredTextRules() {
      if (!this.ratingFilters) return this.currentCustomerRating.text.rules;
      return this.currentCustomerRating.text.rules.filter(rule => this.ratingFilters.includes(rule.rating));
    },

    filteredImagesRules() {
      console.info('images length ' + this.currentCustomerRating.images.length);
      if (!this.ratingFilters) return this.currentCustomerRating.images;
      const imagesRules = [];
      for (let i = 0; i < this.currentCustomerRating.images.length; i++) {
        let imageRule = this.currentCustomerRating.images[i];
        imageRule = JSON.parse(JSON.stringify(imageRule));
        imageRule.rules = imageRule.rules.filter(rule => this.ratingFilters.includes(rule.rating));
        imagesRules.push(imageRule);
      }
      return imagesRules;
    },

    colourToValue(colour) {
      switch (colour) {
        case 'RED':
          return 0;
        case 'AMBER':
          return 1;
        case 'GREEN':
          return 2;
        case 'N/A':
          return 3;
        default:
          return 4;
      }
    },

    askNewUrl() {
      this.askNewUrlDoc = '';
      this.isAskNewUrlDocModalShowing = true;
      this.$nextTick(() => {
        const inputText = document.getElementById('newUrl');
        if (inputText) {
          inputText.value = '';
          inputText.focus();
        }
      });
    },

    hideAskNewUrl() {
      this.isAskNewUrlDocModalShowing = false;
    },

    async analyseNewUrl() {
      console.log('analyseNewUrl');
      const url = document.getElementById('newUrl').value;
      const forceRescan = document.querySelector('#forceRescan').checked;
      const params = forceRescan ? ['forceRescan=true'] : [];
      this.fileMeta.url = this.normalizeUrl(url, params);
      this.hideAskNewUrl();
      this.article = {};
      this.imagesRules = [];
      this.currentCustomerRating = {};
      await this.loadArticle();
    },

    normalizeUrl(url, params) {
      if (!url) {
        return '';
      }
      if (url.toLowerCase().startsWith('http://')) {
        url = url.substring(7);
      }
      if (url.toLowerCase().startsWith('https://')) {
        url = url.substring(8);
      }
      url = url.replace(/%2F/g, '/');
      const paramsString = (params || []).reduce((accumulator, currentValue) => accumulator + '&' + currentValue, '');
      return url + paramsString;
    },

    updateFeedbackIcons(spinner, prefix = '') {
      console.info('updateFeedbackIcons(' + spinner + ', ' + prefix + ')');

      let hasFeedback = false;
      for (let i = 0; i < this.article.ratings.length; i++) {
        const rating = this.article.ratings[i];

        if (rating.expectedRating) {
          hasFeedback = true;
        }

        const ratingColors = ['RED', 'AMBER', 'GREEN', 'N/A'];
        for (let j = 0; j < ratingColors.length; j++) {
          const color = ratingColors[j];

          const icon = document.getElementById(prefix + 'rating_' + rating.customer + '_' + color);
          console.info('getElementById(' + prefix + 'rating_' + rating.customer + '_' + color + ') = ' + icon);

          if (color == rating.rating || color == rating.expectedRating) {
            icon.style.display = '';
            if (spinner) {
              icon.className = 'fas fa-spinner';
              icon.title = 'saving feedback';
            } else if (rating.rating == rating.expectedRating) {
              icon.className = 'far fa-check-circle';
              icon.title = 'Rating confirmed';
            } else if (color == rating.rating) {
              // icon.className = "far fa-times-circle";
              icon.className = 'fas fa-robot';
              icon.title = 'Rating calculated by Watson';
            } else {
              //            				icon.className = "fas fa-exclamation-circle";
              icon.className = 'fas fa-user';
              icon.title = 'Rating set by human';
            }
          } else {
            icon.style.display = 'none';
          }
        }
      }

      document.getElementById('confirmFeedbackBtn').disabled = hasFeedback;
    },

    async feedbackConfirm() {
      for (let i = 0; i < this.article.ratings.length; i++) {
        const rating = this.article.ratings[i];
        rating.expectedRating = rating.rating;
        rating.comment = null;
      }
      this.updateFeedbackIcons(true);
      const response = await apiServer.sendFeedback(this.article, this.creds);
      if (response.dbID) {
        this.article.dbID = response.dbID;
        this.updateFeedbackIcons(false);
      } else {
        // handle error
      }
    },

    async feedbackChange() {
      // confirm any rating not explicitly changed
      for (let i = 0; i < this.article.ratings.length; i++) {
        const rating = this.article.ratings[i];
        if (!rating.expectedRating) {
          rating.expectedRating = rating.rating;
          rating.comment = null;
        }

        const comment = document.getElementById('modal_rating-summary_comment_' + rating.customer);
        rating.comment = comment.value;
      }

      this.updateFeedbackIcons(true);
      this.updateFeedbackIcons(true, 'modal_');
      const response = await apiServer.sendFeedback(this.article, this.creds);
      if (response.dbID) {
        this.article.dbID = response.dbID;
        this.updateFeedbackIcons(false);
      } else {
        // handle error
      }
      this.hideAskFeedback();
    },

    askFeedback() {
      this.isAskFeedbackModalShowing = true;
      this.updateFeedbackIcons(false, 'modal_');
      this.$nextTick(() => {
        const inputText = document.getElementById('newUrl');
        if (inputText) {
          inputText.value = '';
          inputText.focus();
        }
      });
    },

    hideAskFeedback() {
      this.isAskFeedbackModalShowing = false;
    },

    setExpectedRating(customer, expectedRating) {
      for (let i = 0; i < this.article.ratings.length; i++) {
        const rating = this.article.ratings[i];

        if (rating.customer == customer) {
          rating.expectedRating = expectedRating;
        }
      }
      this.updateFeedbackIcons(false, 'modal_');
    },

    hasFeedback() {
      if (this.article && this.article.ratings) {
        for (let i = 0; i < this.article.ratings.length; i++) {
          const rating = this.article.ratings[i];

          if (rating.expectedRating) {
            return true;
          }
        }
      }
      return false;
    }
  }
});
