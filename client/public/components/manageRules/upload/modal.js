Vue.component('tab-manage-rules-upload-modal', {
  props: {
    credentials: {
      type: Object
    },
    recalculationService: {
      type: Boolean,
      Default: false
    }
  },
  mounted() {
    if (!this.init) {
    }
    this.init = true;

    const self = this;
    console.log('modal mounted');
    this.setUploadButtonState();
    $(document).on('change', '.custom-file-input', function(event) {
      $(this)
        .next('.custom-file-label')
        .html(event.target.files[0].name);
    });
  },
  data() {
    return {
      init: false,
      canRecalculate: false,
      fromDate: null,
      toDate: null,
      articleCount: null,
      loadingMessage: 'Loading..',
      errorMessage: 'Failed to complete the request',
      errorTitle: 'Error'
    };
  },
  methods: {
    setUploadButtonState() {
      $('#uploadButton').prop('disabled', !(this.isDateValid() && this.hasUploadedFile()));
    },
    isDateValid() {
      if (this.canRecalculate && this.fromDate && this.toDate && this.fromDate.getTime() >= this.toDate.getTime()) {
        return false;
      }
      return true;
    },
    displayDateAlert() {
      if (!this.isDateValid()) {
        $('#dateAlert').slideDown();
      } else {
        $('#dateAlert').slideUp();
      }
    },
    hasUploadedFile() {
      return document.getElementById('rulesFileToUpload').files.length > 0;
    },
    displayUploadFileAlert() {
      if (!this.hasUploadedFile()) {
        $('#fileAlert').slideDown();
      } else {
        $('#fileAlert').slideUp();
      }
    },
    updateFromDate(date) {
      console.log(`fromDate: ${date}`);
      this.fromDate = date ? new Date(date) : null;
      this.displayDateAlert();
      this.setUploadButtonState();
    },
    updateToDate(date) {
      console.log(`toDate: ${date}`);
      this.toDate = date ? new Date(date) : null;
      this.displayDateAlert();
      this.setUploadButtonState();
    },
    async submit() {
      let canSubmit = true;
      if (!this.isDateValid()) {
        this.displayDateAlert();
        canSubmit = false;
      }
      if (!this.hasUploadedFile()) {
        this.displayUploadFileAlert();
        canSubmit = false;
      }

      if (!canSubmit) {
        return;
      }
      this.loadingMessage = 'Uploading the ruleset...';
      $('#loadingSpinnerModal').modal({
        backdrop: 'static', //remove ability to close modal with click
        keyboard: false, //remove option to close with keyboard
        show: true //Display loader!
      });
      const file = document.getElementById('rulesFileToUpload').files[0];
      const response = await apiServer.uploadRules(file, this.credentials);
      this.isLoading = false;
      if (response.message && !this.canRecalculate) {
        this.$emit('manage-rules-nav-ruleset-uploaded', 'complete');
        setTimeout(() => {
          $('#loadingSpinnerModal').modal('hide');
          setTimeout(() => {
            $('#manageRulesetsUploadModal').modal('hide');
          }, 100);
        }, 500);
      } else if (response.message && this.canRecalculate) {
        this.$emit('manage-rules-nav-ruleset-uploaded', 'complete');
        this.loadingMessage = 'Starting Article processing';
        await this.startRecalculation();
      } else if (response.error) {
        // delay dismissal as it was dismissing too quickly sometimes
        setTimeout(() => {
          $('#loadingSpinnerModal').modal('hide');
          this.errorMessage = response.error;
          setTimeout(() => {
            $('#error-modal-upload').modal('show');
          }, 100);
        }, 500);
      } else {
        setTimeout(() => {
          $('#loadingSpinnerModal').modal('hide');
          this.errorMessage = response;
          setTimeout(() => {
            $('#error-modal-upload').modal('show');
          }, 100);
        }, 500);
      }
    },
    async startRecalculation() {
      const options = {
        fromDate: !this.fromDate || this.fromDate.length === 0 ? null : this.fromDate.toISOString(),
        toDate: !this.toDate || this.toDate.length === 0 ? null : this.toDate.toISOString(),
        numberOfArticles: !this.articleCount || this.articleCount.length === 0 || this.articleCount === '0' ? null : parseInt(this.articleCount, 10)
      };
      response = await apiServer.startRatingsRecalculation(options, this.credentials);
      if (response.message) {
        this.$emit('manage-rules-nav-ruleset-recalc-started', 'inProgress');
        setTimeout(() => {
          $('#loadingSpinnerModal').modal('hide');
          setTimeout(() => {
            $('#manageRulesetsUploadModal').modal('hide');
          }, 100);
        }, 500);
      } else if (response.error) {
        // delay dismissal as it was dismissing too quickly sometimes
        setTimeout(() => {
          $('#loadingSpinnerModal').modal('hide');
          this.errorMessage = `Ruleset was uploaded but failed to start recalculation, please start it manually.\n\nFailed with error: ${response.error.error}`;
          setTimeout(() => {
            $('#error-modal-upload').modal('show');
          }, 100);
        }, 500);
      } else {
        setTimeout(() => {
          $('#loadingSpinnerModal').modal('hide');
          this.errorMessage = `Ruleset was uploaded but failed to start recalculation, please start it manually.\n\nFailed with error: ${response}`;
          setTimeout(() => {
            $('#error-modal-upload').modal('show');
          }, 100);
        }, 500);
      }
    }
  },
  template: `
  <div>
    <div class="modal fade backdrop" id="manageRulesetsUploadModal" tabindex="-1" data-backdrop="static" role="dialog" aria-labelledby="manageRulesetsUploadModalTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="manageRuleUpload">Upload a new ruleset.</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="form-group">
              <div v-if="recalculationService === true">
                <br/>
                <p>Would you like to start processing existent articles with the new ruleset?
                  <input type="checkbox" class="float-right" aria-label="Checkbox to start recalculation" @click="canRecalculate = !canRecalculate;" data-toggle="collapse" href="#collapseRecalControls" role="button" aria-expanded="false" aria-controls="collapseExample">
                </p>
                <div class="card card-body">
                  <div class="alert alert-danger" role="alert" id="fileAlert" style="display:none;">
                    Please upload a valid Excel spreedsheet!!
                  </div>
                  <div class="collapse" id="collapseRecalControls">
                    <div class="alert alert-danger" role="alert" id="dateAlert" style="display:none;">
                      The End date <strong>must</strong> be after the Start date!!
                    </div>
                    <date-picker @update-date-from="updateFromDate" component-suffix="from" placeholder="Start Date (Optional)" v-once/>
                    <date-picker @update-date-to="updateToDate" component-suffix="to" placeholder="End Date (Optional)" v-once/>
                    <div class="input-group mb-3">
                      <input type="text" class="form-control" placeholder="Number of Articles (Optional)" aria-label="articleNumber" aria-describedby="basic-addon1" v-model="articleCount" >
                    </div>
                  </div>
                  <div class="input-group mb-3">
                    <div class="custom-file">
                      <input type="file" class="custom-file-input" id="rulesFileToUpload" aria-describedby="rulesFileToUpload" accept=".xlsx" @change="setUploadButtonState()">
                      <label class="custom-file-label" for="rulesFileToUpload">Choose File</label>
                     </div>
                  </div>
                </div>
              </div>
              <div v-else>
                <br/>
                <div class="input-group mb-3">
                  <div class="custom-file">
                      <input type="file" class="custom-file-input" id="rulesFileToUpload" aria-describedby="rulesFileToUpload" accept=".xlsx" @change="setUploadButtonState">
                      <label class="custom-file-label" for="rulesFileToUpload">Choose File</label>
                    </div>
                  </div>
                </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary" @click="submit()" data-toggle="modal" data-target="#loadingSpinnerModal" id="uploadButton">Upload</button>
          </div>
        </div>
      </div>
    </div>
    <div class="modal fade backdrop" id="loadingSpinnerModal" tabindex="-1" role="dialog" aria-labelledby="manageRulesetsUploadModalTitle" aria-hidden="true" ref="spinnerModal">
      <div class="modal-dialog modal-sm modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-body text-center">
            <div class="loader"></div>
            <div class="loader-text">
              <p>{{ loadingMessage }}</p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <modal-error componentSuffix="upload" v-bind:error-title="errorTitle" v-bind:error-message="errorMessage"></modal-error>
  </div>
`
});
