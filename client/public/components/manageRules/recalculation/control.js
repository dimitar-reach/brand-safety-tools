Vue.component('tab-manage-rules-recalc-control', {
  props: {
    credentials: {
      type: Object
    },
    recalculationService: {
      type: Boolean,
      Default: false
    },
    latestRecalcStatus: {
      type: Object
    }
  },
  data() {
    return {
      init: false,
      recalcObj: null,
      inProgressText: 'The system is currently processing articles, press stop to stop processing articles with the new ruleset.',
      stopText: 'Press Start if you would like to process existent articles with the current uploaded ruleset.',
      infoMessage: 'Failed to complete the request.',
      infoTitle: 'Article processing will stop.'
    };
  },
  watch: {
    latestRecalcStatus: {
      immediate: true,
      handler(val, oldVal) {
        this.recalcObj = val;
      }
    }
  },
  mounted() {
    if (!this.init) {
    }
    this.init = true;
  },
  computed: {
    isStartButtonDisabled() {
      return this.recalcObj && this.recalcObj.status === 'inProgress';
    },
    isStopButtonDisabled() {
      return !this.recalcObj || this.recalcObj.status !== 'inProgress';
    }
  },
  methods: {
    showRecalculationModal() {
      $('#manageRulesetsRecalcModal').modal('show');
    },
    async stopRecalculation() {
      try {
        await apiServer.stopRatingsRecalculation(this.credentials);
        this.$emit('manage-rules-nav-ruleset-updated', 'aborted');
        $('#stopRecalcButton').prop('disabled', true);
        $('#startRecalcButton').prop('disabled', true);
        this.infoMessage = `Recalculation will stop within the next few minutes!!`;
        $('#info-modal-recalc').modal('show');
      } catch (err) {}
    }
  },
  template: `
    <div>
      <div class="card">
        <div class="card-header"><h6 style="padding-top:5pt">Process Articles Ratings</h6></div>
        <div class="card-body">
          <p class="card-text">{{ this.recalcObj && this.recalcObj.status === 'inProgress' ? this.inProgressText : this.stopText }}</p>
          <div class="d-flex justify-content-end">
            <a class="btn btn-primary btn-sm" :class="isStartButtonDisabled ? 'disabled' : ''" :aria-disabled="isStartButtonDisabled" id="startRecalcButton" @click="showRecalculationModal()"><i class="fas fa-play"></i> Start</a>&nbsp
            <a class="btn btn-primary btn-sm" :class="isStopButtonDisabled ? 'disabled' : ''" :aria-disabled="isStopButtonDisabled" @click="stopRecalculation()" id="stopRecalcButton"><i class="fas fa-stop"></i> Stop</a>
          </div>
        </div>
      </div>
      <tab-manage-rules-recalc-modal v-bind:credentials="credentials" v-bind:recalculationService="recalculationService" @start-recalculation="startRecalculation($event)" v-on="$listeners" v-once/>
      <modal-info componentSuffix="recalc" v-bind:title="infoTitle" v-bind:message="infoMessage"></modal-info>
    </div>
`
});
