Vue.component('tab-manage-rules-history', {
  props: {
    credentials: {
      type: Object
    },
    recalculationService: {
      type: Boolean,
      Default: false
    },
    latestRecalcStatus: {
      type: Object
    }
  },
  data() {
    return {
      init: false,
      isLoading: false,
      recalcObj: null,
      recalcUpdates: null,
      statusMessage: 'Loading Ruleset history...',
      modalTitle: 'View Errors'
    };
  },
  watch: {
    latestRecalcStatus: {
      immediate: true,
      handler(val, oldVal) {
        this.getAllRulesetUpdates();
      }
    }
  },
  mounted() {
    if (!this.init) {
    }
    this.init = true;
    this.getAllRulesetUpdates();
  },
  methods: {
    async getAllRulesetUpdates() {
      this.isLoading = true;
      this.statusMessage = 'Loading Rulesets...';
      const response = await apiServer.getAllRecalculationStatus(this.credentials);
      if (response && !response.error) {
        this.recalcUpdates = response;
      } else if (response && response.error) {
        this.recalcUpdates = null;
        if (response.error.error === 'Rating ruleset status not found') {
          this.statusMessage = `No previous ratings recalculation to display`;
        } else {
          this.statusMessage = `Failed to load rulesets updates with error: ${response.error.error}`;
        }
      } else {
        this.recalcUpdates = null;
        this.statusMessage = `Failed to load rulesets updates with error: ${response}`;
      }
      this.isLoading = false;
    },
    buildModalSuffix(recalc) {
      return `errors-${recalc.id}`;
    },
    buildModalClass(recalc) {
      return `#json-viewer-modal-${this.buildModalSuffix(recalc)}`;
    }
  },
  computed: {
    recalcUtils() {
      return recalcUtils;
    }
  },
  template: `
  <div class="row">
  <div class="col">
    <div class="card">
      <div class="card-header">
        <div class="row">
          <div class="col-11"><h6 style="padding-top:5pt">Ratings Processing History</h6></div>
          <div class="col-1"><a class="btn btn-light float-right" @click="getAllRulesetUpdates()"><i class="fas fa-sync"></i></a></div>
        </div>
      </div>
      <div class="card-body">
        <p class="card-text" v-if="isLoading || recalcUpdates === null">{{ this.statusMessage }}</p>
        <table class="table table-striped justify-content-center text-center" v-if="!isLoading && recalcUpdates !== null">
          <thead>
            <tr>
              <th scope="col">Ruleset</th>
              <th scope="col">Started</th>
              <th scope="col">Status</th>
              <th scope="col">Total</th>
              <th scope="col">Complete</th>
              <th scope="col">Errors</th>
              <th scope="col">Rate</th>
              <th scope="col">Elapsed</th>
            </tr>
          </thead>
          <tbody>
            <tr v-for="update in recalcUpdates">
              <td class="status-value text-info">{{ update.rulesetsVersion }}</td>
              <td class="status-value text-info">{{ recalcUtils.startedDate(update) }}</td>
              <td class="status-value" :class="recalcUtils.statusClass(update)">{{ recalcUtils.statusValue(update) }}</td>
              <td>{{ update.expected }}</td>
              <td class="status-value text-success">{{ update.completed }}</td>
              <td class="status-value text-danger"v-if="update.failed === 0">{{ update.failed }}</td>
              <td class="status-value text-danger"v-if="update.failed > 0">
              <a class="status-value text-danger" data-toggle="modal" :data-target="buildModalClass(update)">{{ update.failed }}</a>
                <modal-json-viewer :componentSuffix="buildModalSuffix(update)" v-bind:title="modalTitle" v-bind:json="update.errors"></modal-json-viewer>
              </td>
              <td class="status-value text-info">{{ recalcUtils.rate(update) }}</td>
              <td class="status-value text-info">{{ recalcUtils.duration(update) }}</td>
            </tr>
            </tbody>
        </table>
        <!-- <p class="card-text">table will go here.</p> -->
      </div>
    </div>
  </div>
</div>
`
});
