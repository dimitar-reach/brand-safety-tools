Vue.component('tab-manage-rules-recalc-status', {
  props: {
    credentials: {
      type: Object
    },
    recalculationService: {
      type: Boolean
    },
    latestRecalcStatus: {
      type: Object
    },
    updating: null
  },
  data() {
    return {
      init: false,
      recalcObj: null,
      statusMessage: 'To see updates of the last article ratings processing, please initiate the ratings processor in the menu above or upload a new ruleset.',
      previousProgressValue: 0,
      modalTitle: 'View Errors'
    };
  },
  watch: {
    latestRecalcStatus: {
      immediate: true,
      handler(val, oldVal) {
        this.recalcObj = val;
        this.updateProgress();
      }
    }
  },
  created() {
    console.log('created status');
  },
  mounted() {
    if (!this.init) {
    }
    this.init = true;
  },
  beforeDestroy() {
    clearInterval(this.updating);
  },
  methods: {
    updateProgress() {
      if (this.progressValue !== this.previousProgressValue && this.init) {
        $('#recalc-status-progress-bar')
          .css('width', this.progressValue + '%')
          .attr('aria-valuenow', this.progressValue);
        this.previousProgressValue = this.progressValue;
      }
    },
    async getLatestRulesetStatus() {
      response = await apiServer.getLatestRecalculationStatus(this.credentials);
      if (response && !response.error) {
        this.recalcObj = response;
      } else {
        this.recalcObj = null;
      }
      this.updateProgress();
      return response;
    },
    async reload() {
      this.getLatestRulesetStatus();
    }
  },
  computed: {
    progressValue() {
      if (!this.recalcObj) {
        return 0;
      }
      const complete = this.recalcObj.completed;
      const expected = this.recalcObj.expected;
      return expected === 0 ? 0 : (complete / expected) * 100;
    },
    progressValueStyle() {
      return `width: ${this.progressValue}%`;
    },
    recalcUtils() {
      return recalcUtils;
    }
  },
  template: `
  <div class="row" v-if="init === true">
    <div class="col">
      <div class="card" v-if="recalculationService === true && recalcObj !== null">
        <div class="card-header">
          <div class="row">
            <div class="col-11"><h6 style="padding-top:5pt">Latest Ratings Processing Status</h6></div>
            <div class="col-1"><a class="btn btn-light float-right" @click="reload()"><i class="fas fa-sync"></i></a></div>
          </div>
        </div>
        <div class="card-body" >
          <div class="row">
            <div class="col">
              <p class="status-time">Started: <span class="status-time-value">{{ recalcUtils.startedDate(recalcObj) }}</span></p>
              <div class="progress" style="height: 15px;">
                <div class="progress-bar bg-primary progress-bar-animated" id="recalc-status-progress-bar" role="progressbar" style="height: 15px; margin: 0" :style="progressValueStyle" :aria-valuenow="progressValue" aria-valuemin="0" aria-valuemax="100"></div>
              </div>
              <p class="status-time text-right">{{ recalcObj.status === 'inProgress'? 'Expected: ' : 'Completed: ' }}<span class="status-time-value">{{ recalcUtils.expectedDate(recalcObj) }}</span></p>
            </div>
          </div>
          <br/>
          <div class="row justify-content-center text-center">
            <div class="col-12">
              <table class="status-table w-100 bg-light">
                <tbody>
                  <tr class="">
                    <td><p class="status-header">Ruleset Version</p><p class="status-value text-info">{{ recalcObj.rulesetsVersion }}</p></td>
                    <td><p class="status-header">Total</p><p class="status-value text-info">{{ recalcObj.expected }}</p></td>
                    <td><p class="status-header">Complete</p><p class="status-value text-success">{{ recalcObj.completed }}</p></td>
                    <td>
                      <p class="status-header">Failed</p>
                      <p class="status-value text-danger" v-if="recalcObj.failed === 0">{{ recalcObj.failed }}</p>
                      <a class="status-value text-danger" v-if="recalcObj.failed > 0" data-toggle="modal" data-target="#json-viewer-modal-errors">{{ recalcObj.failed }}</a>
                      <modal-json-viewer componentSuffix="errors" v-bind:title="modalTitle" v-bind:json="recalcObj.errors"></modal-json-viewer>
                    </td>
                    <td><p class="status-header">Rate</p><p class="status-value text-info">{{ recalcUtils.rate(recalcObj) }}</p></td>
                    
                    <td>
                      <p class="status-header date">Status</p><p class="status-value" :class="recalcUtils.statusClass(recalcObj)">{{ recalcUtils.statusValue(recalcObj) }}</p>
                    </td>
                    <td><p class="status-header">Elapsed</p><p class="status-value text-info">{{ recalcUtils.duration(recalcObj) }}</p></td>
                  </tr>
                  <tr class="status-bottom-row">
                    <td class="status-subtitle"></td>
                    <td class="status-subtitle"></td>
                    <td class="status-subtitle"></td>
                    <td class="status-subtitle"></td>
                    <td class="status-subtitle">Articles p/min</td>
                    <td class="status-subtitle"></td>
                    <td class="status-subtitle"></td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    
  </div>
`
});
