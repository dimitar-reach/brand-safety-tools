const recalcUtils = {
  rate(recalc) {
    if (!recalc.creationDate || !recalc.updatedDate || !recalc.completed || recalc.completed === 0) {
      return '0';
    }
    const startDate = new Date(recalc.creationDate);
    const updatedDate = new Date(recalc.updatedDate);

    const duration = (updatedDate.getTime() - startDate.getTime()) / 1000; //secs
    const rate = (recalc.completed / duration) * 60; // p/min

    return rate.toFixed(2);
  },
  statusValue(recalc) {
    if (recalc.status === 'inProgress') {
      return 'In progress';
    }

    return recalc.status.charAt('0').toUpperCase() + recalc.status.slice(1);
  },
  statusClass(recalc) {
    switch (recalc.status) {
      case 'inProgress':
        return 'text-info';
      case 'complete':
        return 'text-success';
      case 'aborted':
      case 'failed':
        return 'text-danger';
    }
  },
  startedDate(recalc) {
    const startDate = new Date(recalc.creationDate);
    return `${startDate.toDateString()}, ${startDate.toLocaleTimeString()}`;
  },
  expectedDate(recalc) {
    if (!recalc || !recalc.creationDate || !recalc.updatedDate || !recalc.completed || recalc.completed === 0) {
      return 'Unknown';
    }
    const updatedDate = new Date(recalc.updatedDate);
    if (recalc.status === 'inProgress') {
      const startDate = new Date(recalc.creationDate);
      const duration = updatedDate.getTime() - startDate.getTime(); //msecs
      const rate = recalc.completed / duration; // p/milisecond

      const expectedDuration = (recalc.expected * duration) / recalc.completed;
      const expectedDate = new Date(startDate.getTime() + expectedDuration);
      return `${expectedDate.toDateString()}, ${expectedDate.toLocaleTimeString()}`;
    }
    return `${updatedDate.toDateString()}, ${updatedDate.toLocaleTimeString()}`;
  },
  duration(recalc) {
    if (!recalc || !recalc.creationDate || !recalc.updatedDate || !recalc.completed || recalc.completed === 0) {
      return 'Unknown';
    }

    const start = moment.utc(recalc.creationDate);
    const end = moment.utc(recalc.updatedDate);

    const duration = moment.duration(end.diff(start));
    if (duration.days() > 0) {
      return `${duration.days()}d ${utils.lpad(`${duration.hours()}`, '0', 2)}h ${utils.lpad(`${duration.minutes()}`, '0', 2)}m ${utils.rpad(
        `${duration.seconds()}`,
        '0',
        2
      )}s`;
    } else if (duration.hours() > 0) {
      return `${utils.lpad(`${duration.hours()}`, '0', 2)}h ${utils.lpad(`${duration.minutes()}`, '0', 2)}m ${utils.rpad(`${duration.seconds()}`, '0', 2)}s`;
    } else if (duration.minutes() > 0) {
      return `${utils.lpad(`${duration.minutes()}`, '0', 2)}m ${utils.rpad(`${duration.seconds()}`, '0', 2)}s`;
    } else if (duration.seconds() > 0) {
      return `${utils.rpad(`${duration.seconds()}`, '0', 2)}s`;
    }
  }
};
