Vue.component('tab-manage-rules-recalc-modal', {
  props: {
    credentials: {
      type: Object
    },
    recalculationService: {
      type: Boolean,
      Default: false
    }
  },
  mounted() {
    if (!this.init) {
    }
    this.init = true;

    const self = this;
    console.log('modal mounted');
    this.setUploadButtonState();
    $(document).on('change', '.custom-file-input', function(event) {
      $(this)
        .next('.custom-file-label')
        .html(event.target.files[0].name);
    });
  },
  data() {
    return {
      init: false,
      canRecalculate: false,
      fromDate: null,
      toDate: null,
      articleCount: null,
      loadingMessage: 'Loading..',
      errorMessage: 'Failed to complete the request',
      errorTitle: 'Error'
    };
  },
  methods: {
    setUploadButtonState() {
      $('#startButton').prop('disabled', !this.isDateValid());
    },
    isDateValid() {
      if (this.fromDate && this.toDate && this.fromDate.getTime() >= this.toDate.getTime()) {
        return false;
      }
      return true;
    },
    displayDateAlert() {
      if (!this.isDateValid()) {
        $('#dateAlertRecalc').slideDown();
      } else {
        $('#dateAlertRecalc').slideUp();
      }
    },
    updateFromDate(date) {
      console.log(`fromDateRecalc: ${date}`);
      this.fromDate = date ? new Date(date) : null;
      this.displayDateAlert();
      this.setUploadButtonState();
    },
    updateToDate(date) {
      console.log(`toDateRecalc: ${date}`);
      this.toDate = date ? new Date(date) : null;
      this.displayDateAlert();
      this.setUploadButtonState();
    },
    async submit() {
      let canSubmit = true;
      if (!this.isDateValid()) {
        this.displayDateAlert();
        canSubmit = false;
      }

      if (!canSubmit) {
        return;
      }
      this.loadingMessage = 'Starting Recalculation...';
      $('#loadingSpinnerModalStart').modal({
        backdrop: 'static', //remove ability to close modal with click
        keyboard: false, //remove option to close with keyboard
        show: true //Display loader!
      });
      await this.startRecalculation();
    },
    async startRecalculation() {
      const options = {
        fromDate: !this.fromDate || this.fromDate.length === 0 ? null : this.fromDate.toISOString(),
        toDate: !this.toDate || this.toDate.length === 0 ? null : this.toDate.toISOString(),
        numberOfArticles: !this.articleCount || this.articleCount.length === 0 || this.articleCount === '0' ? null : parseInt(this.articleCount, 10)
      };
      response = await apiServer.startRatingsRecalculation(options, this.credentials);
      if (response.message) {
        this.$emit('manage-rules-nav-ruleset-updated', 'inProgress');
        setTimeout(() => {
          $('#loadingSpinnerModalStart').modal('hide');
          setTimeout(() => {
            $('#manageRulesetsRecalcModal').modal('hide');
          }, 100);
        }, 500);
      } else if (response.error) {
        // delay dismissal as it was dismissing too quickly sometimes
        setTimeout(() => {
          $('#loadingSpinnerModalStart').modal('hide');
          this.errorMessage = `Failed to start recalculation.\n\nFailed with error: ${response.error.error}`;
          setTimeout(() => {
            $('#error-modal-recalc').modal('show');
          }, 100);
        }, 500);
      } else {
        setTimeout(() => {
          $('#loadingSpinnerModalStart').modal('hide');
          this.errorMessage = `Failed to start recalculation.\n\nFailed with error: ${response}`;
          setTimeout(() => {
            $('#error-modal-recalc').modal('show');
          }, 100);
        }, 500);
      }
    }
  },
  template: `
  <div>
    <div class="modal fade backdrop" id="manageRulesetsRecalcModal" tabindex="-1" data-backdrop="static" role="dialog" aria-labelledby="manageRulesetsUploadModalTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="manageRuleUpload">Process Articles</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="form-group">
              <div class="card card-body">
                <div class="alert alert-danger" role="alert" id="dateAlertRecalc" style="display:none;">
                  The End date <strong>must</strong> be after the Start date!!
                </div>
                <date-picker @update-date-from-recalc="updateFromDate" component-suffix="from-recalc" placeholder="Start Date (Optional)" v-once/>
                <date-picker @update-date-to-recalc="updateToDate" component-suffix="to-recalc" placeholder="End Date (Optional)" v-once/>
                <div class="input-group mb-3">
                  <input type="text" class="form-control" placeholder="Number of Articles (Optional)" aria-label="articleNumber" aria-describedby="basic-addon1" v-model="articleCount" >
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary" @click="submit()" data-toggle="modal" data-target="#loadingSpinnerModalStart" id="startButton">Start</button>
          </div>
        </div>
      </div>
    </div>
    <div class="modal fade backdrop" id="loadingSpinnerModalStart" tabindex="-1" role="dialog" aria-labelledby="manageRulesetsUploadModalTitle" aria-hidden="true" ref="spinnerModal">
      <div class="modal-dialog modal-sm modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-body text-center">
            <div class="loader"></div>
            <div class="loader-text">
              <p>{{ loadingMessage }}</p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <modal-error componentSuffix="recalc" v-bind:error-title="errorTitle" v-bind:error-message="errorMessage"></modal-error>
  </div>
`
});
