const EventBus = new Vue();
Vue.component('ruleset-tab-navigation', {
  props: {
    recalculationService: {
      type: Boolean,
      Default: false
    },
    activeIndex: {
      type: Number,
      Default: 0
    }
  },
  data() {
    return {
      tabs: [
        {
          link: 'manageRules.html',
          title: 'Home',
          component: 'manage-rules-home'
        },
        {
          link: 'manageRulesStats.html',
          title: 'Statistics',
          component: 'manage-rules-stats'
        }
      ],
      recalcServiceTab: {
        link: 'manageRulesHistory.html',
        title: 'Recalculation History',
        component: 'manage-rules-history'
      },
      recalcService: this.recalculationService,
      tabIndex: this.activeIndex
    };
  },
  mounted() {
    if (this.recalculationService) {
      this.tabs.push(this.recalcServiceTab);
    }
  },
  watch: {
    recalculationService: {
      immediate: true,
      handler(val, oldVal) {
        const index = this.tabs.findIndex(tab => tab.link === 'manageRulesHistory.html');
        if (val === false) {
          if (index !== -1) {
            this.tabs.splice(index, 1);
          }
        } else if (val === true && val !== this.recalcService && index !== -1) {
          this.tabs.push(this.recalcServiceTab);
        }
        this.recalcService = val;
      }
    }
  },
  methods: {
    changeActive(index) {
      // The view model.
      console.log(`clicked with index: ${index}`);
      this.tabIndex = index;
      this.$emit('manage-rules-nav-updated', this.tabs[this.tabIndex].component);
    },
    getId(element) {
      return `v-pills-${element.title.toLowerCase()}-tab`;
    },
    getAriaControls(element) {
      return `v-pills-${element.title.toLowerCase()}`;
    },
    isActive(index) {
      return index === this.tabIndex;
    }
  },
  template: `
  <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
    <a v-for="(element, index) in tabs" class="nav-link" :id="getId(element)" data-toggle="pill" :class="{ 'active': index === tabIndex }" href="#" @click="changeActive(index)" role="tab" :on-click:aria-controls="getAriaControls(element)">{{ element.title }}</a>
  </div> 
`
});
