Vue.component('tab-manage-rules-rulesets-list', {
  props: {
    credentials: {
      type: Object
    },
    recalculationService: {
      type: Boolean,
      Default: false
    },
    latestRecalcStatus: {
      type: Object
    },
    rulesetChanged: {
      type: Number
    }
  },
  data() {
    return {
      init: false,
      rulesets: [],
      isLoading: false,
      statusMessage: 'Loading Rulesets...'
    };
  },
  watch: {
    rulesetChanged: {
      immediate: true,
      handler(val, oldVal) {
        if (val && val !== oldVal) {
          this.getRatingsRulesets();
        }
      }
    }
  },
  mounted() {
    if (!this.init) {
    }
    this.init = true;
    this.getRatingsRulesets();
  },
  methods: {
    async getRatingsRulesets() {
      this.isLoading = true;
      this.statusMessage = 'Loading Rulesets...';
      const response = await apiServer.getRatingsRulesets(10, this.credentials);
      if (response && !response.error) {
        this.rulesets = response;
      } else if (response && response.error) {
        this.rulesets = null;
        this.statusMessage = `Failed to load rulesets with error: ${response.error.error}`;
      } else {
        this.rulesets = null;
        this.statusMessage = `Failed to load rulesets with error: ${response}`;
      }
      this.isLoading = false;
    },
    getRulesetFile(version) {
      return `/ruleset/ratings/file/version/${version}`;
    },
    buildCreationDate(ruleset) {
      const startDate = new Date(ruleset.creationDate);
      return `${startDate.toDateString()}, ${startDate.toLocaleTimeString()}`;
    }
  },
  template: `
  <div class="row">
    <div class="col">
      <div class="card">
        <div class="card-header">
          <div class="row">
            <div class="col-11"><h6 style="padding-top:5pt">Rulesets</h6></div>
            <div class="col-1"><a class="btn btn-light float-right" @click="getRatingsRulesets()"><i class="fas fa-sync"></i></a></div>
          </div>
        </div>
        <div class="card-body">
          <p class="card-text" v-if="isLoading || rulesets === null">{{ this.statusMessage }}</p>
          <table class="table table-striped justify-content-center text-center" v-if="!isLoading && rulesets !== null">
            <thead>
              <tr>
                <th scope="col">Version</th>
                <th scope="col">Creation Date</th>
                <th scope="col">Uploaded by</th>
                <th scope="col">Ruleset</th>
              </tr>
            </thead>
            <tbody>
              <tr >
              <tr v-for="ruleset in rulesets">
                <td>{{ ruleset.version }}</td>
                <td>{{ buildCreationDate(ruleset) }}</td>
                <td>{{ ruleset.userID }}</td>
                <a :href="getRulesetFile(ruleset.version)" class="btn btn-primary btn-sm table-button">Download</a>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
`
});
