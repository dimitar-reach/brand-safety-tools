/* eslint-disable object-shorthand */
Vue.component('tab-manage-rules-stats', {
  props: {
    credentials: {
      type: Object
    }
  },
  data() {
    return {
      init: false,
      isLoading: true,
      ruleStats: [{ version: 'Loading..' }],
      customerCount: [],
      customerList: [],
      customerObjects: {},
      viewDataVersion: 1
    };
  },
  mounted() {
    if (!this.init) {
      this.getArticlesByRulesetVersion();
    }
  },
  methods: {
    buildDate(date) {
      if (!date && date.length === 0) {
        return '';
      }
      const startDate = new Date(date);
      return `${startDate.toDateString()}, ${startDate.toLocaleTimeString()}`;
    },
    reload() {
      this.isLoading = true;
      this.ruleStats = [{ version: 'Loading..' }];
      this.init = false;
      this.getArticlesByRulesetVersion();
    },
    hasRulesFileToUpload() {
      return !document.getElementById('rulesFileToUpload').value;
    },
    async getArticlesByRulesetVersion() {
      fetch('/ruleset/ratings/version/articleCount')
        .then(response => response.json())
        .then(data => {
          this.loading = false;
          this.ruleStats = data.rows.reverse();
          this.init = true;
        });
    },
    async getCustomerCount(event, version) {
      this.viewDataVersion = version;
      this.customerObjects = {};
      this.customerCount = [];
      this.customerList = [];
      fetch('/ruleset/ratings/version/' + version + '/customerCount')
        .then(response => response.json())
        .then(data => {
          this.loading = false;
          this.customerCount = data.rows.reverse();
          let customer = '';
          this.customerCount.forEach(customerData => {
            if (customer !== customerData.customer) {
              customer = customerData.customer;
              this.customerList.push(customerData.customer);
            }
          });
          customer = '';
          this.customerCount.forEach(customerData => {
            this.setCustomerPercentage(customerData, '');
          });
          this.customerCount.forEach(customerData => {
            if (customer !== customerData.customer) {
              this.setTotals(customerData, '', true);
              customer = customerData.customer;
            }
          });
        });
      if (version - 1 > 0) {
        fetch('/ruleset/ratings/version/' + (version - 1) + '/customerCount')
          .then(response => response.json())
          .then(data => {
            const previousVersionCustomerCount = data.rows.reverse();
            let customer = '';
            previousVersionCustomerCount.forEach(c => {
              this.setCustomerPercentage(c, '_PREV');
            });
            previousVersionCustomerCount.forEach(customerData => {
              if (customer !== customerData.customer) {
                this.setTotals(customerData, '_PREV', false);
                customer = customerData.customer;
              }
            });
            this.customerCount = this.customerCount.map(c => {
              const diff =
                parseFloat(this.customerObjects[c.customer][c.calculated_rating + '_PERC']) -
                parseFloat(this.customerObjects[c.customer][c.calculated_rating + '_PERC_PREV']);

              c.diff = 'Not enough data';
              if (diff > 0) {
                c.diff = '+' + diff.toFixed(2);
              }
              if (diff < 0) {
                c.diff = diff.toFixed(2);
              }
              return c;
            });
          });
      }
    },
    displayChart(customerData, red, green, amber) {
      setTimeout(() => {
        new Chart('chart-' + customerData.customer, {
          type: 'doughnut',
          data: {
            labels: ['Red', 'Green', 'Amber'],
            datasets: [
              {
                label: '# of articles',
                data: [red, green, amber],
                backgroundColor: ['#c72f2f', '#499e1d', '#9e5e1d'],
                borderColor: ['#c72f2f', '#499e1d', '#9e5e1d'],
                borderWidth: 1
              }
            ]
          },
          options: {
            responsive: true,
            cutoutPercentage: 40,
            legend: {
              display: false
            },
            title: {
              display: true,
              text: customerData.customer
            },
            animation: {
              duration: 1000,
              animateScale: true,
              animateRotate: true
            }
          }
        });
      }, 500);
    },
    setCustomerPercentage(customerData, suffix) {
      if (!this.customerObjects[customerData.customer]) {
        this.customerObjects[customerData.customer] = {
          RED: 0,
          GREEN: 0,
          AMBER: 0,
          RED_PREV: 0,
          GREEN_PREV: 0,
          AMBER_PREV: 0,
          RED_PERC: '0',
          GREEN_PERC: '0',
          AMBER_PERC: '0',
          RED_PERC_PREV: '0',
          GREEN_PERC_PREV: '0',
          AMBER_PERC_PREV: '0'
        };
      }
      this.customerObjects[customerData.customer][customerData.calculated_rating + suffix] = customerData.articles_count;
    },
    setTotals(customerData, suffix, showChart) {
      const red = this.customerObjects[customerData.customer]['RED' + suffix];
      const green = this.customerObjects[customerData.customer]['GREEN' + suffix];
      const amber = this.customerObjects[customerData.customer]['AMBER' + suffix];
      const total = parseInt(red) + parseInt(green) + parseInt(amber);

      this.customerObjects[customerData.customer]['RED_PERC' + suffix] = parseFloat((red / total) * 100).toFixed(2);
      this.customerObjects[customerData.customer]['GREEN_PERC' + suffix] = parseFloat((green / total) * 100).toFixed(2);
      this.customerObjects[customerData.customer]['AMBER_PERC' + suffix] = parseFloat((amber / total) * 100).toFixed(2);

      if (showChart) {
        this.displayChart(customerData, red, green, amber);
      }
    },
    async postData(url = '', data = {}) {
      // Default options are marked with *
      const response = await fetch(url, {
        method: 'POST', // *GET, POST, PUT, DELETE, etc.
        mode: 'cors', // no-cors, *cors, same-origin
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'same-origin', // include, *same-origin, omit
        headers: {
          'Content-Type': 'application/json'
          // 'Content-Type': 'application/x-www-form-urlencoded',
        },
        redirect: 'follow', // manual, *follow, error
        referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
        body: JSON.stringify(data) // body data type must match "Content-Type" header
      });
      return response.json(); // parses JSON response into native JavaScript objects
    }
  },
  template: `
    <div class="row">
      <div class="col">
        <div class="card">
          <div class="card-header">
            <div class="row">
              <div class="col-11"><h6 style="padding-top:5pt">Statistics</h6></div>
              <div class="col-1"><a class="btn btn-light float-right" @click="reload()"><i class="fas fa-sync"></i></a></div>
            </div>
          </div>
          <div class="card-body">
            <table class="table table-striped">
              <thead>
                <tr>
                  <td>Version</td>
                  <td>Creation date</td>
                  <td>User ID</td>
                  <td>Articles count</td>
                  <td>View data</td>
                </tr>
              </thead>
              <tbody>
                <tr v-for="ruleset in ruleStats" :key="ruleset.version">
                  <td>{{ ruleset.version }}</td>
                  <td>{{ buildDate(ruleset.creation_date) }}</td>
                  <td>{{ ruleset.user_id }}</td>
                  <td>{{ ruleset.articles_count }}</td>
                  <td>
                    <a href="#" data-toggle="modal" data-target="#viewDataContainer" @click="getCustomerCount($event, ruleset.version)"
                      ><i class="fa fa-chart-bar"></i>
                    </a>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    <div class="modal fade" id="viewDataContainer" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">View Data for Version {{viewDataVersion}}</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="charts-container">
              <div class="chart" v-for="customer in customerList">
                <canvas v-bind:id="'chart-'+customer" width="200" height="200"> </canvas>
              </div>
            </div>
            <br />
            <br />
            <table class="table table-striped">
              <tr>
                <td>Customer</td>
                <td>Flag</td>
                <td>Articles</td>
                <td>% of Total</td>
                <td>Previous version</td>
                <td>Changed (in %)</td>
              </tr>
              <tr v-for="customerData in customerCount">
                <td>{{ customerData.customer}}</td>
                <td>{{ customerData.calculated_rating}}</td>
                <td>{{ customerData.articles_count}}</td>
                <td>{{ customerObjects[customerData.customer][customerData.calculated_rating+'_PERC']}}</td>
                <td>{{ customerObjects[customerData.customer][customerData.calculated_rating+'_PERC_PREV'] || 'Not enough data'}}</td>
                <td>{{ customerData.diff }}</td>
              </tr>
            </table>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>
  </div>
`
});

// :aria-selected="isActive(index)"
