new Vue({
  el: '#manage-rules',
  data: {
    defaultHomeTab: 'manage-rules-home',
    currentTab: 'manage-rules-home',
    polling: null,
    credentials: {
      username: 'reach',
      password: 'watson'
    },
    latestRecalcStatus: null,
    rulesetChanged: null,
    isLoading: false
  },
  created() {
    this.onChangedTab(this.defaultHomeTab);
    this.pollData();
  },
  beforeDestroy() {
    clearInterval(this.polling);
  },
  methods: {
    hasStatusChanged(newStatus) {
      if ((newStatus && !this.latestRecalcStatus) || (!newStatus && this.latestRecalcStatus)) {
        return true;
      }

      if (newStatus && this.latestRecalcStatus) {
        if (newStatus.id !== this.latestRecalcStatus.id) {
          return true;
        }
        if (newStatus.updatedDate !== this.latestRecalcStatus.updatedDate) {
          return true;
        }
      }

      return false;
    },
    pollData() {
      this.polling = setInterval(() => {
        this.getLatestRulesetStatus();
      }, 10 * 1000);
    },
    async onRulesetUpload(status) {
      this.rulesetChanged++;
    },
    async onRecalcStarted(status) {
      await this.getLatestRulesetStatus();
    },
    async onChangedTab(newValue) {
      console.log(`changed tab: ${newValue}`);
      this.currentTab = newValue;
      if (this.currentTab === this.defaultHomeTab && this.isRecalculationService) {
        await this.getLatestRulesetStatus();
      }
    },
    async getLatestRulesetStatus() {
      response = await apiServer.getLatestRecalculationStatus(this.credentials);
      if (response && !response.error) {
        if (this.hasStatusChanged(response)) {
          this.latestRecalcStatus = response;
        }
      } else {
        this.latestRecalcStatus = null;
      }
      return response;
    }
  },
  computed: {
    isRecalculationService() {
      return getFeatureList().recalculationService || false;
    },
    currentTabComponent() {
      return 'tab-' + this.currentTab.toLowerCase();
    }
  }
});
