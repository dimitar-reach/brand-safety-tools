Vue.component('tab-manage-rules-home', {
  props: {
    credentials: {
      type: Object
    },
    recalculationService: {
      type: Boolean,
      Default: false
    },
    latestRecalcStatus: {
      type: Object
    },
    rulesetChanged: {
      type: Number
    }
  },
  data() {
    return {
      init: false
    };
  },
  mounted() {
    if (document.cookie.indexOf('configuration_service=true') !== -1) {
      window.location.href = '/admin.html';
    } else {
      if (!this.init) {
      }
      this.init = true;
    }
  },
  template: `
    <div>
      <div class="row">
        <div class="col-sm-6">
          <tab-manage-rules-upload-card v-bind:credentials="credentials" v-bind:recalculation-service="recalculationService" v-bind:latest-recalc-status="latestRecalcStatus" v-on="$listeners"/>
        </div>
        <div class="col-sm-6" v-if="recalculationService === false">
          <div class="card">
            <div class="card-header"><h6 style="padding-top:5pt">Download</h6></div>
            <div class="card-body">
              <p class="card-text">Tap the Download button to get latest Ruleset.</p>
              <a href="/rules" class="btn btn-primary btn-sm">Download</a>
            </div>
          </div>
        </div>
        <div class="col-sm-6" v-else>
          <tab-manage-rules-recalc-control v-bind:credentials="credentials" v-bind:recalculation-service="recalculationService" v-bind:latest-recalc-status="latestRecalcStatus" v-on="$listeners"/>
        </div>
      </div>
      <br/>
      <tab-manage-rules-recalc-status v-bind:credentials="credentials" v-bind:recalculation-service="recalculationService" v-bind:latest-recalc-status="latestRecalcStatus" v-on="$listeners"/>
      <br/>
      <tab-manage-rules-rulesets-list v-bind:credentials="credentials" v-bind:recalculation-service="recalculationService" v-bind:latest-recalc-status="latestRecalcStatus" v-bind:ruleset-changed="rulesetChanged" v-on="$listeners"/>
    </div>
`
});
