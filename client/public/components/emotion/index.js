/* eslint-disable object-shorthand */
Vue.component('emotion-table', {
  props: {
    emotion: {
      type: Object,
      required: true
    }
  },
  methods: {
    getEmotionKeys: function(emotion) {
      return Object.keys(emotion);
    }
  },
  computed: {
    emotionComponents() {
      const headers = this.getEmotionKeys(this.emotion);
      const values = [];
      const levels = [];
      for (value of headers) {
        values.push(this.emotion[value]);
      }
      return { headers, values };
    }
  },
  template: `
  <div>
    <table>
      <thead>
        <th v-for="(header) in emotionComponents.headers" class="emotion_header">{{ _.capitalize(header) }}</th>
      </thead>
      <tbody>
        <tr>
          <td v-for="(emotionObj) in emotionComponents.values" class="emotion_value">
            <span class="emotion_value_normal">{{ emotionObj.level }}</span>
            <span class="emotion_value_small">{{ emotionObj.value.toFixed(3) }}</span>
          </td>
        </tr>
      </tbody>
    </table>
  </div>
`
});
