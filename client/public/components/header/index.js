Vue.component('app-header', {
  data() {
    return {};
  },
  methods: {
    isSuperAdmin() {
      return document.cookie.includes('userrole=superadmin');
    },
    isAdmin() {
      return document.cookie.includes('userrole=admin') || document.cookie.includes('userrole=superadmin');
    }
  },
  props: ['title'],
  template:
    '<div class="header header-top">' +
    '<nav class="navbar navbar-expand-lg">' +
    '<a href="/"><img class="title__logo" src="./assets/M-Logo.jpg" /></a>' +
    '<div class="collapse navbar-collapse" id="navbarSupportedContent">' +
    '<ul class="navbar-nav mr-auto">' +
    '<li class="nav-item"><a class="nav-link" href="/"><i class="fas fa-home"></i> Home</a></li>' +
    '<li class="nav-item" v-if="isAdmin()"><a class="nav-link" href="/manageRules.html"><i class="fas fa-tasks"></i> Edit rules</a></li>' +
    '<li class="nav-item"><a class="nav-link" href="/showStats.html"><i class="fas fa-chart-line"></i> Statistics</a></li>' +
    '<li class="nav-item" v-if="isSuperAdmin()"><a class="nav-link" href="/admin.html"><i class="fas fa-cogs"></i> Configuration</a></li>' +
    '<li class="nav-item" v-if="isAdmin()"><a class="nav-link" href="/marketPlaceMappings.html"><i class="fas fa-tags"></i>Marketplace mappings</a></li>' +
    '</ul></div>' +
    '</nav>' +
    '</div>'
});
