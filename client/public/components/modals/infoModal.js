Vue.component('modal-info', {
  props: {
    title: {
      type: String,
      require: true,
      default: 'Info'
    },
    message: {
      type: String,
      require: true
    },
    buttonTitle: {
      type: String,
      default: 'OK!'
    },
    componentSuffix: {
      type: String,
      require: true
    }
  },
  computed: {
    idStr() {
      return `info-modal-${this.componentSuffix}`;
    }
  },
  template: `
  <div class="modal fade" :id="idStr" tabindex="-1" role="dialog" aria-labelledby="infoModalMessage" aria-hidden="true" ref="errorModal">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header bg-info text-white">{{ title }}</div>
        <div class="modal-body text-center">
            <br/>
            <p>{{ message }}</p>
        </div>
        <div class="modal-footer text-center">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ buttonTitle }}</button>
        </div>
      </div>
    </div>
  </div>
`
});
