Vue.component('modal-error', {
  props: {
    errorTitle: {
      type: String,
      require: true,
      default: 'Error'
    },
    errorMessage: {
      type: String,
      require: true
    },
    buttonTitle: {
      type: String,
      default: 'OK!'
    },
    componentSuffix: {
      type: String,
      require: true
    }
  },
  computed: {
    idStr() {
      return `error-modal-${this.componentSuffix}`;
    }
  },
  template: `
  <div class="modal fade" :id="idStr" tabindex="-1" role="dialog" aria-labelledby="errorModalMessage" aria-hidden="true" ref="errorModal">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header bg-danger text-white">{{ errorTitle }}</div>
        <div class="modal-body text-center">
            <br/>
            <p>{{ errorMessage }}</p>
        </div>
        <div class="modal-footer text-center">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ buttonTitle }}</button>
        </div>
      </div>
    </div>
  </div>
`
});
