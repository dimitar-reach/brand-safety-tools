Vue.component('modal-json-viewer', {
  props: {
    title: {
      type: String,
      require: true,
      default: 'Info'
    },
    json: {
      require: true
    },
    editable: {
      type: Boolean,
      require: false,
      default: false
    },
    mode: {
      type: String,
      default: 'code' // view
    },
    componentSuffix: {
      type: String,
      require: true
    },
    buttonTitle: {
      type: String,
      default: 'OK!'
    }
  },
  watch: {
    json: {
      immediate: true,
      handler(val) {
        if (this.editor) {
          this.editor.update(val);
        }
      }
    }
  },
  data() {
    return {
      editor: null
    };
  },
  mounted() {
    const self = this;
    const container = document.getElementById(this.jsonEditorId);
    const options = {
      mode: this.mode,
      modes: ['text', 'code', 'view'],
      indentation: 2,
      search: false,
      onEditable(node) {
        if (!node.path) {
          // In modes code and text, node is empty: no path, field, or value
          // returning false makes the text area read-only
          return self.editable;
        }
      },
      onModeChange(newMode, oldMode) {
        console.log('Mode switched from', oldMode, 'to', newMode);
      }
    };
    this.editor = new JSONEditor(container, options);
    this.editor.set(this.json);
  },
  computed: {
    idStr() {
      return `json-viewer-modal-${this.componentSuffix}`;
    },
    jsonEditorId() {
      return `jsoneditor-${this.componentSuffix}`;
    }
  },
  template: `
  <div class="modal fade" :id="idStr" tabindex="-1" role="dialog" aria-labelledby="infoModalMessage" aria-hidden="true" ref="errorModal">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header bg-light text-primary">{{ title }}</div>
        <div class="modal-body text-center" style="margin-top: 20pt">
          <div :id="jsonEditorId" style="width: 700px; height: 600px;"></div>
        </div>
        <div class="modal-footer text-center">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ buttonTitle }}</button>
        </div>
      </div>
  </div>
  </div>
`
});
