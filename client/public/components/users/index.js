const configurationPage = new Vue({
  el: '#app-users',
  filters: {},
  data: {
    message: '',
    error: '',
    modalContent: {},
    modalTitle: '',
    params: [],
    processUpdateFlag: false,
    processInsertFlag: false,
    loading: false,
    updateValue: '',
    updateType: 'string',
    processClasses: 'btn btn-primary',
    insertName: '',
    insertValue: '',
    insertAppid: '',
    insertType: 'string'
  },
  computed: {},
  async mounted() {
    this.getAllParams();
  },
  watch: {},
  methods: {
    //async example() {}
    getAllParams() {
      this.loading = true;
      this.postData('/getParams', {}).then(data => {
        this.loading = false;
        this.params = data.rows;
      });
    },
    editButtonClicked(event, title, id, name, value, type, appid) {
      this.modalTitle = title;
      this.updateId = id;
      this.updateValue = value;
      this.updateType = type;
      // this.modalSaveButton = '<button type="button" class="btn btn-primary" @click="alert(1)" data-dismiss="modal">Save changes</button>';
      this.modalContent = [
        { label: 'ID', text: id },
        { label: 'Name', text: name },
        { label: 'Type', text: type },
        { label: 'App ID', text: appid },
        { label: 'Value', fieldId: 'update-value', fieldValue: value }
      ];
    },
    async postData(url = '', data = {}) {
      // Default options are marked with *
      const response = await fetch(url, {
        method: 'POST', // *GET, POST, PUT, DELETE, etc.
        mode: 'cors', // no-cors, *cors, same-origin
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'same-origin', // include, *same-origin, omit
        headers: {
          'Content-Type': 'application/json'
          // 'Content-Type': 'application/x-www-form-urlencoded',
        },
        redirect: 'follow', // manual, *follow, error
        referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
        body: JSON.stringify(data) // body data type must match "Content-Type" header
      });
      return response.json(); // parses JSON response into native JavaScript objects
    },
    processInsert() {
      this.processClasses = 'btn btn-primary disabled';

      if (this.processInsertFlag || !this.insertName || !this.insertValue || !this.insertAppid || !this.insertType) {
        this.clearAll();
        this.error = 'Please enter value in all fields';
        return false;
      }
      this.processInsertFlag = true;
      this.loading = true;
      this.postData('/insertParams', {
        name: this.insertName,
        value: this.insertValue,
        appid: this.insertAppid,
        type: this.insertType
      })
        .then(data => {
          this.processInsertFlag = false;
          if (data.error) {
            this.clearAll();
            this.error = data.error;
          } else {
            this.clearAll();
            this.message = 'Successfully saved';
            this.getAllParams();
          }
        })
        .catch(e => {
          this.clearAll();
          this.error = e.message;
        });
      return false;
    },
    processDelete(id) {
      const confirmDel = confirm('Are you sure you want to delete?' + id);
      if (this.processUpdateFlag || !id || !confirmDel) {
        return false;
      }
      this.processUpdateFlag = true;
      this.postData('/deleteParam', {
        id
      }).then(data => {
        this.clearAll();
        this.error = 'Param deleted';
        this.processUpdateFlag = false;
        this.getAllParams();
      });
      return false;
    },
    processUpdate() {
      if (this.processUpdateFlag || !this.updateId || !this.updateValue) {
        this.clearAll();
        this.error = 'Please enter value. If you want to delete it, press Close and then click on the trash icon.';
        return false;
      }
      this.loading = true;
      this.processUpdateFlag = true;
      this.postData('/setParams', {
        id: this.updateId,
        type: this.updateType,
        value: this.updateValue
      })
        .then(data => {
          this.processUpdateFlag = false;
          if (data.error) {
            this.clearAll();
            this.error = data.error;
          } else {
            this.clearAll();
            this.message = 'Successfully updated';
            this.getAllParams();
          }
        })
        .catch(e => {
          this.clearAll();
          this.error = e.message;
        });
      return false;
    },
    clearAll() {
      this.message = '';
      this.error = '';
      this.processClasses = 'btn btn-primary';
      this.insertName = '';
      this.insertValue = '';
      this.insertAppid = '';
      this.insertType = 'string';
    }
  }
});
