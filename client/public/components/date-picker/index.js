// from https://vuejsdevelopers.com/2017/05/20/vue-js-safely-jquery-plugin/
Vue.component('date-picker', {
  props: {
    dateFormat: {
      type: String,
      default: 'yy-mm-dd'
    },
    minDate: {
      type: String,
      default: '-1Y'
    },
    maxDate: {
      type: String,
      default: '0'
    },
    componentSuffix: {
      type: String,
      required: true
    },
    placeholder: {
      type: String,
      default: ''
    }
  },
  mounted() {
    var self = this;
    $(this.$refs.input).datepicker({
      dateFormat: this.dateFormat,
      minDate: this.minDate,
      maxDate: this.maxDate,
      showButtonPanel: true,
      closeText: 'Clear',
      onClose(dateText, inst) {
        if ($(window.event.target).hasClass('ui-datepicker-close')) {
          document.getElementById(self.idStr).value = '';
          self.$emit(`update-date-${self.componentSuffix}`, null);
        }
      },
      onSelect(date) {
        console.log(`updated date in: update-date-${self.componentSuffix}: `, date);
        self.$emit(`update-date-${self.componentSuffix}`, date);
      }
    });
  },
  beforeDestroy() {
    $(this.$refs.input)
      .datepicker('hide')
      .datepicker('destroy');
  },
  computed: {
    idStr() {
      return `${this.componentSuffix}-date-picker`;
    }
  },
  template: `
  <div class="input-group mb-3">
    <div class="input-group-prepend">
      <label class="input-group-text" for="inputGroupSelect01"><i class="fas fa-calendar-alt"/></label>
    </div>
    <input type="text" :id="idStr" class="form-control" :placeholder="placeholder" aria-label="" aria-describedby="basic-addon1" ref="input">
  </div>
  `
});
