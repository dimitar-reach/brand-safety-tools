const xandrMappings = new Vue({
    el: '#app',
    filters: {},
    data: {
      message: '',
      error: '',
      modalContent: {},
      credentials: {
        username: 'reach',
        password: 'watson'
      },
      modalTitle: '',
      params: [],
      processUpdateFlag: false,
      processInsertFlag: false,
      loading: false,
      updateValue: '',
      updateType: 'string',
      processClasses: 'btn btn-primary',
      insertName: '',
      insertValue: '',
      insertAppid: '',
      insertType: 'string',
      records: [],
      insertNewRecord: {name:"dynamic_categories_map"},
      selectedTable: {},
      tables: [
        {
          id: 'params',
          name: 'Params',
          columns: [
            { name: 'id' },
            { name: 'name' },
            { name: 'value', variable: true },
            { name: 'appid' },
            { name: 'type', type: 'dropdown', options: ['string', 'number', 'object'] }
          ],
          getEndpoint: '/getParams',
          setEndpoint: '/setParams',
          deleteEndpoint: '/deleteParam',
          insertEndpoint: '/insertParams'
        },
        {
          id: 'user_roles',
          name: 'User Roles',
          columns: [
            { name: 'id', type: 'increment' },
            { name: 'role_name', variable: true },
            { name: 'scopes', variable: true }
          ],
          getEndpoint: '/user-roles/get',
          insertEndpoint: '/user-roles/create'
        },
        {
          id: 'users',
          name: 'Users',
          columns: [
            { name: 'id' },
            { name: 'username', variable: true },
            { name: 'password' },
            { name: 'email', variable: true },
            { name: 'user_role', variable: true }
          ],
          getEndpoint: '/users/get',
          insertEndpoint: '/user/create'
        }
      ],
      configTables: [
        {
          id: 'apps',
          name: 'Apps',
          columns: [{ name: 'id' }, { name: 'endpoint' }, { name: 'description' }],
          getEndpoint: '/apps/get'
        },
        {
          id: 'publication_groups',
          name: 'Publication groups',
          columns: [
            { name: 'id' },
            { name: 'base_urls' },
            { name: 'description' },
            { name: 'selectors' },
            { name: 'transformations' },
            { name: 'shared_publications_config' },
            { name: 'appid' }
          ],
          getEndpoint: '/publications/get'
        },
        {
          id: 'matching_criteria',
          name: 'Matching Criteria',
          columns: [
            { name: 'id' },
            { name: 'send_to_apps' },
            { name: 'jexl_transformation_query' },
            { name: 'jexl_transformation_filters' },
            { name: 'matching_segments' },
            { name: 'save_to_bucket' }
          ],
          getEndpoint: '/matching-criteria/get'
        },
        {
          id: 'webhooks',
          name: 'Webhooks',
          columns: [
            { name: 'id' },
            { name: 'function_type' },
            { name: 'endpoint' },
            { name: 'method_type' },
            { name: 'expected_response' },
            { name: 'timeout' },
            { name: 'authentication' },
            { name: 'frequency_minute' },
            { name: 'appid' },
            { name: 'retry' }
          ],
          getEndpoint: '/webhooks/get'
        }
      ]
    },
    async mounted() {
      this.selectedTable = this.tables[0];
      if (document.cookie.indexOf('configuration_service=true') > -1) {
        this.tables = this.tables.concat(this.configTables);
      }
      this.getAllParams();
    },
    watch: {},
    components: {
      sanderUploadCard: sanderUploadCard,
      uploadModal: uploadModal,
      mappingsList: mappingsList
     },
    methods: {
      //async example() {}
      setSelectedTab(tableId) {
        this.selectedTable = this.tables.filter(table => table.id === tableId)[0];
        this.loading = true;
        this.postData(this.selectedTable.getEndpoint, {})
          .then(data => {
            this.loading = false;
            this.records = data.rows || [];
          })
          .catch(e => {
            console.log(e);
            this.records = [];
          });
      },
      getAllParams() {
        this.loading = true;
        this.postData('/getParams', {}).then(data => {
          this.loading = false;
          this.records = data.rows;
        });
      },
      editButtonClicked(event, title, record) {
        this.modalTitle = title;
        this.updateId = record.id;
  
        // this.modalSaveButton = '<button type="button" class="btn btn-primary" @click="alert(1)" data-dismiss="modal">Save changes</button>';
        this.modalContent = this.selectedTable.columns.map(column => {
          const value = record[column.name];
          return {
            label: column.name.charAt(0).toUpperCase() + column.name.slice(1),
            fieldId: 'update-' + column.name,
            fieldValue: value,
            variable: column.variable
          };
        });
      },
      async postData(url = '', data = {}) {
        // Default options are marked with *
        const response = await fetch(url, {
          method: 'POST', // *GET, POST, PUT, DELETE, etc.
          mode: 'cors', // no-cors, *cors, same-origin
          cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
          credentials: 'same-origin', // include, *same-origin, omit
          headers: {
            'Content-Type': 'application/json'
            // 'Content-Type': 'application/x-www-form-urlencoded',
          },
          redirect: 'follow', // manual, *follow, error
          referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
          body: JSON.stringify(data) // body data type must match "Content-Type" header
        });
        return response.json(); // parses JSON response into native JavaScript objects
      },
      processInsert() {
        this.processClasses = 'btn btn-primary disabled';
        if (
          this.processInsertFlag &&
          this.selectedTable.columns.filter(column => this.insertNewRecord[column.name]).length === this.selectedTable.columns.length + 1
        ) {
          this.clearAll();
          this.error = 'Please enter value in all fields';
          return false;
        }
        this.processInsertFlag = true;
        this.loading = true;
        const incrementColumn = this.selectedTable.columns.filter(column => column.type === 'increment')[0];
        if (incrementColumn) {
          this.insertNewRecord[incrementColumn.name] = this.records.length + 1;
        }
        this.postData(this.selectedTable.insertEndpoint, this.insertNewRecord)
          .then(data => {
            this.processInsertFlag = false;
            if (data.error) {
              this.clearAll();
              this.error = data.error;
            } else {
              this.setSelectedTab(this.selectedTable.id);
              this.clearAll();
              this.message = 'Successfully saved';
            }
          })
          .catch(e => {
            this.clearAll();
            this.error = e.message;
          });
        return false;
      },
      processDelete(id) {
        const confirmDel = confirm('Are you sure you want to delete?' + id);
        if (this.processUpdateFlag || !id || !confirmDel) {
          return false;
        }
        this.processUpdateFlag = true;
        this.postData(this.selectedTable.deleteEndpoint, {
          id
        }).then(data => {
          this.clearAll();
          this.error = 'Record deleted';
          this.processUpdateFlag = false;
          this.getAllParams();
        });
        return false;
      },
      processUpdate() {
        const selectedKeysValues = this.modalContent.reduce((acc, content) => {
          acc[content.fieldId.replace('update-', '')] = content.fieldValue;
          return acc;
        }, {});
        if (this.processUpdateFlag || !this.updateId) {
          this.clearAll();
          this.error = 'Please enter value. If you want to delete it, press Close and then click on the trash icon.';
          return false;
        }
        this.loading = true;
        this.processUpdateFlag = true;
        this.postData(this.selectedTable.setEndpoint, selectedKeysValues)
          .then(data => {
            this.processUpdateFlag = false;
            if (data.error) {
              this.clearAll();
              this.error = data.error;
            } else {
              this.clearAll();
              this.message = 'Successfully updated';
              this.getAllParams();
            }
          })
          .catch(e => {
            this.clearAll();
            this.error = e.message;
          });
        return false;
      },
      clearAll() {
        this.message = '';
        this.error = '';
        this.processClasses = 'btn btn-primary';
        this.insertName = '';
        this.insertValue = '';
        this.insertAppid = '';
        this.insertType = 'string';
        this.insertNewRecord = {};
      }
    }
  });
  