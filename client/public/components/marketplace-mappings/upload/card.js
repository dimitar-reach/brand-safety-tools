const sanderUploadCard = Vue.component('marketplace-upload-card', {
    props: {
      credentials: {
        type: Object
      },
      recalculationService: {
        type: Boolean
      },
      latestRecalcStatus: {
        type: Object
      }
    },
    data() {
      return {
        init: false,
        inProgressInterval: 60 * 10 * 1000,
        errorTitle: 'Ruleset in Progress.',
        errorMessage: 'The system is already updating all articles with the latest ruleset. Please stop this process before uploading a new Ruleset.'
      };
    },
    mounted() {
      if (!this.init) {
      }
      this.init = true;
    },
    methods: {
      loadModal() {
        $('#manageRulesetsUploadModal').modal('show');
      }
    },
    template: `
      <div>
        <modal-error componentSuffix="inprogress" v-bind:error-title="errorTitle" v-bind:error-message="errorMessage"></modal-error>
        <div class="card">
          <div class="card-header"><h6 style="padding-top:5pt">Upload</h6></div>
          <div class="card-body">
            <p class="card-text">Marketplace mapping is in excel format, to upload a new file, tap Upload! </p>
            <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" @click="loadModal()">Upload</button>
          </div>
        </div>
        <tab-manage-rules-upload-modal v-bind:credentials="credentials" v-bind:recalculationService="recalculationService" v-on="$listeners" v-once/>
      </div>
  `
  });
  