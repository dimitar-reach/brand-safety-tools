
const mappingsList = Vue.component('mappings-list', {
    props: {
        credentials: {
            type: Object
        }
    },
    data() {
        return {
            init: false,
            mappings: [],
            isLoading: false,
            statusMessage: 'Loading mappings...'
        };
    },
    watch: {
        getMappingFiles: {
            immediate: true,
            handler(val, oldVal) {
                if (val && val !== oldVal) {
                    this.getMappingFiles();
                }
            }
        }
    },
    mounted() {
        if (!this.init) {
        this.init = true;
      }
    },
    methods: {
        async getMappingFiles() {
            this.isLoading = true;
            this.statusMessage = 'Loading mapping files...';
            const response = await apiServer.getMarketplaceMappingFiles(20, this.credentials);
            if (response && !response.error) {
                this.mappings = response;
            } else if (response && response.error) {
                this.mappings = null;
                this.statusMessage = `Failed to load mappings with error: ${response.error.error}`;
            } else {
                this.mappings = null;
                this.statusMessage = `Failed to load mappings with error: ${response}`;
            }
            this.isLoading = false;
        },
        getMappingFile(version) {
            return `/marketplaceMapping/file/version/${version}`;
        },
        async deleteFile(version){

          const response = await apiServer.deleteMarketplaceMappingFile(version, this.credentials);
          console.log(response);

        },
        buildCreationDate(ruleset) {
            const startDate = new Date(ruleset.update_date);
            return `${startDate.toDateString()}, ${startDate.toLocaleTimeString()}`;
        }
    },
    template: `
    <div class="row">
      <div class="col">
        <div class="card">
          <div class="card-header">
            <div class="row">
              <div class="col-11"><h6 style="padding-top:5pt">Market mapping files</h6></div>
              <div class="col-1"><a class="btn btn-light float-right" @click="getMappingFiles()"><i class="fas fa-sync"></i></a></div>
            </div>
          </div>
          <div class="card-body">
            <p class="card-text" v-if="isLoading || mappings === null">{{ this.statusMessage }}</p>
            <table class="table table-striped justify-content-center text-center" v-if="!isLoading && mappings !== null">
              <thead>
                <tr>
                  <th scope="col">Version</th>
                  <th scope="col">Update Date</th>
                  <th scope="col">Uploaded by</th>
                  <th scope="col">File</th>
                </tr>
              </thead>
              <tbody>
                <tr >
                <tr v-for="file in mappings">
                  <td>{{ file.version }}</td>
                  <td>{{ buildCreationDate(file) }}</td>
                  <td>{{ file.user_id }}</td>
                  <a :href="getMappingFile(file.version)" class="btn btn-primary btn-sm table-button">Download</a>
                  &nbsp;
                  <button @click="deleteFile(file.version)" class="btn btn-warning btn-sm table-button">Delete</button>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  `
});
