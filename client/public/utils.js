const utils = {
  lpad(str, padString, length) {
    while (str.length < length) str = padString + str;
    return str;
  },
  rpad(str, padString, length) {
    while (str.length < length) str = str + padString;
    return str;
  }
};
