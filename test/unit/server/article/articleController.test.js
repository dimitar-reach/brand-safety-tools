const mockLoadModule = require('../../../mocks/loadModule.mock');

let articleRouteController = null;

const createMockResponse = evaluator => ({
  userID: 'testUserId',
  set: jest.fn(),
  status: jest.fn(),
  userID: 'testUserId',
  // eslint-disable-next-line object-shorthand
  send: function(response) {
    if (evaluator) {
      evaluator(response, this);
    }
  }
});

const createMockResponseObject = input => ({
  input,
  findings: {
    text: {},
    images: []
  },
  ratings: [
    {
      customer: 'Default',
      rating: 'GREEN',
      images: [],
      ruleSetVersion: 2
    }
  ]
});

describe('test articleController', () => {
  beforeEach(() => {
    jest.mock('../../../../loadModule', () => mockLoadModule);
    articleRouteController = require('../../../../server/routes/article/articleController');
  });

  afterEach(() => {
    mockLoadModule.reset();
    jest.resetModules();
    jest.resetModuleRegistry();
    jest.clearAllTimers();
    jest.unmock('../../../../loadModule');
  });

  test('getClassification() controller with no id, cms_id or url, returning error', done => {
    const mockRequest = {
      query: {}
    };

    const evalExpect = (response, mockResponse) => {
      expect(mockLoadModule.objectUtils.buildResponse.init).toBeCalledTimes(0);
      expect(mockResponse.status).toBeCalledWith(200);
      expect(response).toMatchObject({ error: 'a required param (url, cmsID, dbID) was not passed' });
      done();
    };

    const mockResponse = createMockResponse(evalExpect);
    try {
      articleRouteController.getClassification(mockRequest, mockResponse);
    } catch (e) {
      console.log(e);
    }
  });

  test('getClassification() controller with valid input, with params ratingsOnly=false and responseType=sanitised, but orchestrator returns error', done => {
    const mockRequest = {
      query: {
        dbID: 'test-id'
      },
      headers: {}
    };
    mockLoadModule.objectUtils.string.isJson.mockReturnValueOnce(true);

    mockLoadModule.analyzer.orchestrator.getCachedArticleFindings.mockResolvedValueOnce({ error: 'article Id not found' });
    mockLoadModule.objectUtils.buildResponse.init.mockReturnValueOnce({ error: 'article Id not found' });
    const expectedOrchestrationInput = {
      input: {
        dbID: 'test-id',
        cmsID: undefined,
        url: undefined
      }
    };

    const evalExpect = (response, mockResponse) => {
      expect(mockLoadModule.objectUtils.buildResponse.init).toBeCalledTimes(1);
      expect(mockLoadModule.objectUtils.buildResponse.init).toBeCalledWith({ error: 'article Id not found' }, undefined, 'sanitised');
      expect(mockResponse.status).toBeCalledWith(200);
      expect(mockLoadModule.analyzer.orchestrator.getCachedArticleFindings).toBeCalledWith(expectedOrchestrationInput, 'testUserId');
      expect(response).toMatchObject({ error: 'article Id not found' });
      done();
    };

    const mockResponse = createMockResponse(evalExpect);
    articleRouteController.getClassification(mockRequest, mockResponse, false, 'sanitised');
  });

  test('getClassification() controller with valid input, ith params ratingsOnly=false and responseType=sanitised, orchestrator returns findings', done => {
    const mockRequest = {
      query: {
        dbID: 'test-id'
      },
      headers: {}
    };

    const orchestrationResolved = createMockResponseObject({
      input: {
        dbID: 'test-id',
        cmsID: undefined,
        url: undefined
      }
    });

    const expectedOrchestrationInput = {
      input: {
        dbID: 'test-id',
        cmsID: undefined,
        url: undefined
      }
    };

    mockLoadModule.objectUtils.string.isJson.mockReturnValueOnce(true);
    mockLoadModule.analyzer.orchestrator.getCachedArticleFindings.mockResolvedValueOnce(orchestrationResolved);
    mockLoadModule.objectUtils.buildResponse.init.mockReturnValueOnce(orchestrationResolved);
    const evalExpect = (response, mockResponse) => {
      expect(mockLoadModule.objectUtils.buildResponse.init).toBeCalledTimes(1);
      expect(mockLoadModule.objectUtils.buildResponse.init).toBeCalledWith(orchestrationResolved, undefined, 'sanitised');
      expect(mockResponse.status).toBeCalledWith(200);
      expect(mockLoadModule.analyzer.orchestrator.getCachedArticleFindings).toBeCalledWith(expectedOrchestrationInput, 'testUserId');
      expect(response).toMatchObject(orchestrationResolved);
      done();
    };

    const mockResponse = createMockResponse(evalExpect);
    articleRouteController.getClassification(mockRequest, mockResponse, false, 'sanitised');
  });

  test('getClassification() controller with valid input, request query parameter with ratingsOnly=true, params ratingsOnly=false and responseType=sanitised, orchestrator returns findings', done => {
    const mockRequest = {
      query: {
        dbID: 'test-id',
        ratingsOnly: 'true'
      },
      headers: {}
    };

    const orchestrationResolved = createMockResponseObject({
      input: {
        canRecalculate: undefined,
        dbID: 'test-id',
        cmsID: undefined,
        url: undefined,
        exactVersion: undefined,
        filter: ['ratings'],
        lastModified: undefined
      }
    });

    const expectedOrchestrationInput = {
      input: {
        canRecalculate: undefined,
        dbID: 'test-id',
        cmsID: undefined,
        url: undefined,
        exactVersion: undefined,
        filter: ['ratings'],
        lastModified: undefined
      }
    };

    mockLoadModule.objectUtils.string.isJson.mockReturnValueOnce(true);
    mockLoadModule.analyzer.orchestrator.getCachedArticleFindings.mockResolvedValueOnce(orchestrationResolved);
    mockLoadModule.objectUtils.buildResponse.init.mockReturnValueOnce(orchestrationResolved);

    const evalExpect = (response, mockResponse) => {
      expect(mockLoadModule.analyzer.orchestrator.getCachedArticleFindings).toBeCalledWith(expectedOrchestrationInput, 'testUserId');
      expect(mockLoadModule.objectUtils.buildResponse.init).toBeCalledTimes(1);
      expect(mockLoadModule.objectUtils.buildResponse.init).toBeCalledWith(orchestrationResolved, ['ratings'], 'sanitised');
      expect(mockResponse.status).toBeCalledWith(200);
      expect(response).toMatchObject(orchestrationResolved);
      done();
    };

    const mockResponse = createMockResponse(evalExpect);
    articleRouteController.getClassification(mockRequest, mockResponse, false, 'sanitised');
  });

  test('getClassification() controller with valid input with filters, but input ratingsOnly parameter flag is true and responseType=sanitised, orchestrator returns ratings', done => {
    const mockRequest = {
      query: {
        dbID: 'test-id',
        filters: 'sentiment,emotion'
      },
      headers: {}
    };

    const orchestrationResolved = createMockResponseObject({
      input: {
        canRecalculate: undefined,
        dbID: 'test-id',
        cmsID: undefined,
        url: undefined,
        lastModified: undefined
      }
    });

    const expectedOrchestrationInput = {
      input: {
        canRecalculate: undefined,
        dbID: 'test-id',
        cmsID: undefined,
        url: undefined,
        lastModified: undefined,
        exactVersion: undefined,
        filter: undefined
      }
    };

    mockLoadModule.objectUtils.string.filtersFromString.mockReturnValueOnce(['sentiment', 'emotion']);
    mockLoadModule.objectUtils.string.isJson.mockReturnValueOnce(true);
    mockLoadModule.analyzer.orchestrator.getCachedArticleFindings.mockResolvedValueOnce(orchestrationResolved);
    mockLoadModule.objectUtils.buildResponse.init.mockReturnValueOnce(orchestrationResolved);
    const evalExpect = (response, mockResponse) => {
      expect(mockResponse.status).toBeCalledWith(200);
      expect(mockLoadModule.objectUtils.buildResponse.init).toBeCalledTimes(1);
      expect(mockLoadModule.objectUtils.buildResponse.init).toBeCalledWith(orchestrationResolved, ['ratings'], 'sanitised');
      expect(mockLoadModule.analyzer.orchestrator.getCachedArticleFindings).toBeCalledWith(expectedOrchestrationInput, 'testUserId');
      expect(response).toMatchObject(orchestrationResolved);
      done();
    };

    const mockResponse = createMockResponse(evalExpect);
    articleRouteController.getClassification(mockRequest, mockResponse, true, 'sanitised');
  });

  test('getClassification() controller with valid input but using filter flag, orchestrator returns valid findings, params ratingsOnly=false and responseType=sanitised, returns filtered findings', done => {
    const mockRequest = {
      query: {
        dbID: 'test-id',
        filter: 'ratings,sentiment,emotion'
      },
      headers: {}
    };

    const orchestrationResolved = createMockResponseObject({
      input: {
        canRecalculate: undefined,
        dbID: 'test-id',
        cmsID: undefined,
        url: undefined,
        filter: ['ratings', 'sentiment', 'emotion'],
        lastModified: undefined,
        exactVersion: undefined
      }
    });

    const expectedOrchestrationInput = {
      input: {
        canRecalculate: undefined,
        dbID: 'test-id',
        cmsID: undefined,
        url: undefined,
        filter: ['ratings', 'sentiment', 'emotion'],
        lastModified: undefined,
        exactVersion: undefined
      }
    };

    mockLoadModule.objectUtils.string.filtersFromString.mockReturnValueOnce(['ratings', 'sentiment', 'emotion']);
    mockLoadModule.objectUtils.string.isJson.mockReturnValueOnce(true);
    mockLoadModule.analyzer.orchestrator.getCachedArticleFindings.mockResolvedValueOnce(orchestrationResolved);
    mockLoadModule.objectUtils.buildResponse.init.mockReturnValueOnce({ filteredResponse: 'true' });
    const evalExpect = (response, mockResponse) => {
      expect(mockResponse.status).toBeCalledWith(200);
      expect(mockLoadModule.objectUtils.buildResponse.init).toBeCalledTimes(1);
      expect(mockLoadModule.objectUtils.buildResponse.init).toBeCalledWith(orchestrationResolved, ['ratings', 'sentiment', 'emotion'], 'sanitised');
      expect(mockLoadModule.analyzer.orchestrator.getCachedArticleFindings).toBeCalledWith(expectedOrchestrationInput, 'testUserId');
      expect(response).toMatchObject({ filteredResponse: 'true' });
      done();
    };

    const mockResponse = createMockResponse(evalExpect);

    articleRouteController.getClassification(mockRequest, mockResponse, false, 'sanitised');
  });

  test('getClassification() controller with valid input with timeout query, which causes a timeout', done => {
    jest.useFakeTimers();
    const mockRequest = {
      query: {
        dbID: 'test-id',
        timeout: '700'
      },
      headers: {}
    };

    const orchestrationResolved = createMockResponseObject({
      input: {
        dbID: 'test-id',
        cmsID: undefined,
        url: undefined
      }
    });

    const expectedOrchestrationInput = {
      input: {
        dbID: 'test-id',
        cmsID: undefined,
        url: undefined
      }
    };

    mockLoadModule.objectUtils.string.isJson.mockReturnValueOnce(true);
    mockLoadModule.analyzer.orchestrator.getCachedArticleFindings.mockImplementation(
      () => new Promise(resolve => setTimeout(() => resolve(orchestrationResolved), 2000))
    );
    let doneCalled = false;
    const evalExpect = (response, mockResponse) => {
      if (!doneCalled) {
        expect(mockLoadModule.objectUtils.buildResponse.init).toBeCalledTimes(0);
        expect(mockResponse.status).toBeCalledWith(200);
        expect(mockLoadModule.analyzer.orchestrator.getCachedArticleFindings).toBeCalledWith(expectedOrchestrationInput, 'testUserId');
        expect(response).toMatchObject({ error: 'Request has timed out' });
        doneCalled = true;
        done();
      }
    };

    const mockResponse = createMockResponse(evalExpect);
    articleRouteController.getClassification(mockRequest, mockResponse, false, 'sanitised');
    jest.advanceTimersByTime(2000);
  });

  test('classifyArticle() controller with valid input, but orchestrator returns error', done => {
    const parameters = {
      input: {
        dbID: 'test-id'
      }
    };

    mockLoadModule.analyzer.orchestrator.analyzeHtml.mockResolvedValueOnce({ error: 'article Id not found' });
    mockLoadModule.objectUtils.buildResponse.init.mockReturnValueOnce({ error: 'article Id not found' });
    const expectedOrchestrationInput = {
      input: {
        dbID: 'test-id',
        cmsID: undefined,
        url: undefined
      }
    };

    const evalExpect = (response, mockResponse) => {
      expect(mockLoadModule.objectUtils.buildResponse.init).toBeCalledTimes(1);
      expect(mockLoadModule.objectUtils.buildResponse.init).toBeCalledWith({ error: 'article Id not found' }, undefined, 'sanitised');
      expect(mockResponse.status).toBeCalledWith(200);
      expect(mockLoadModule.analyzer.orchestrator.analyzeHtml).toBeCalledWith(expectedOrchestrationInput, 'testUserId', undefined);
      expect(response).toMatchObject({ error: 'article Id not found' });
      done();
    };

    const mockResponse = createMockResponse(evalExpect);
    articleRouteController.classifyArticle({ query: {}, headers: {} }, mockResponse, parameters, 'sanitised');
  });

  test('classifyArticle() controller with valid input, orchestrator returns findings', done => {
    const parameters = {
      input: {
        dbID: 'test-id'
      }
    };

    const orchestrationResolved = createMockResponseObject({
      input: {
        dbID: 'test-id',
        cmsID: undefined,
        url: undefined
      }
    });

    const expectedOrchestrationInput = {
      input: {
        dbID: 'test-id',
        cmsID: undefined,
        url: undefined
      }
    };

    mockLoadModule.objectUtils.string.isJson.mockReturnValueOnce(true);
    mockLoadModule.analyzer.orchestrator.analyzeHtml.mockResolvedValueOnce(orchestrationResolved);
    mockLoadModule.objectUtils.buildResponse.init.mockReturnValueOnce(orchestrationResolved);
    const evalExpect = (response, mockResponse) => {
      expect(mockLoadModule.objectUtils.buildResponse.init).toBeCalledTimes(1);
      expect(mockLoadModule.objectUtils.buildResponse.init).toBeCalledWith(orchestrationResolved, undefined, 'sanitised');
      expect(mockResponse.status).toBeCalledWith(200);
      expect(mockLoadModule.analyzer.orchestrator.analyzeHtml).toBeCalledWith(expectedOrchestrationInput, 'testUserId', undefined);
      expect(response).toMatchObject(orchestrationResolved);
      done();
    };

    const mockResponse = createMockResponse(evalExpect);
    articleRouteController.classifyArticle({ query: {}, headers: {} }, mockResponse, parameters, 'sanitised');
  });

  test('classifyArticle() controller with valid input but ratingsOnly=true, orchestrator returns findings', done => {
    const mockRequest = {
      query: {
        dbID: 'test-id',
        ratingsOnly: 'true'
      },
      headers: {}
    };

    const parameters = {
      input: {
        dbID: 'test-id'
      }
    };

    const orchestrationResolved = createMockResponseObject({
      input: {
        dbID: 'test-id',
        cmsID: undefined,
        url: undefined
      }
    });

    const expectedOrchestrationInput = {
      input: {
        dbID: 'test-id',
        cmsID: undefined,
        url: undefined
      }
    };

    mockLoadModule.objectUtils.string.isJson.mockReturnValueOnce(true);
    mockLoadModule.analyzer.orchestrator.analyzeHtml.mockResolvedValueOnce(orchestrationResolved);
    mockLoadModule.objectUtils.buildResponse.init.mockReturnValueOnce(orchestrationResolved);

    const evalExpect = (response, mockResponse) => {
      expect(mockLoadModule.analyzer.orchestrator.analyzeHtml).toBeCalledWith(expectedOrchestrationInput, 'testUserId', undefined);
      expect(mockLoadModule.objectUtils.buildResponse.init).toBeCalledTimes(1);
      expect(mockLoadModule.objectUtils.buildResponse.init).toBeCalledWith(orchestrationResolved, ['ratings'], 'sanitised');
      expect(mockResponse.status).toBeCalledWith(200);
      expect(response).toMatchObject(orchestrationResolved);
      done();
    };

    const mockResponse = createMockResponse(evalExpect);
    articleRouteController.classifyArticle(mockRequest, mockResponse, parameters, 'sanitised');
  });

  test('classifyArticle() controller with valid input but using filter flag, orchestrator returns valid findings, returns filtered findings', done => {
    const mockRequest = {
      query: {
        dbID: 'test-id',
        filter: 'ratings,sentiment,emotion'
      },
      headers: {}
    };

    const parameters = {
      input: {
        dbID: 'test-id'
      }
    };

    const orchestrationResolved = createMockResponseObject({
      input: {
        dbID: 'test-id',
        cmsID: undefined,
        url: undefined
      }
    });

    const expectedOrchestrationInput = {
      input: {
        dbID: 'test-id',
        cmsID: undefined,
        url: undefined
      }
    };

    mockLoadModule.objectUtils.string.isJson.mockReturnValueOnce(true);
    mockLoadModule.objectUtils.string.filtersFromString.mockReturnValueOnce(['ratings', 'sentiment', 'emotion']);
    mockLoadModule.analyzer.orchestrator.analyzeHtml.mockResolvedValueOnce(orchestrationResolved);
    mockLoadModule.objectUtils.buildResponse.init.mockReturnValueOnce({ filteredResponse: 'true' });
    const evalExpect = (response, mockResponse) => {
      expect(mockResponse.status).toBeCalledWith(200);
      expect(mockLoadModule.objectUtils.buildResponse.init).toBeCalledTimes(1);
      expect(mockLoadModule.objectUtils.buildResponse.init).toBeCalledWith(orchestrationResolved, ['ratings', 'sentiment', 'emotion'], 'sanitised');
      expect(mockLoadModule.analyzer.orchestrator.analyzeHtml).toBeCalledWith(expectedOrchestrationInput, 'testUserId', undefined);
      expect(response).toMatchObject({ filteredResponse: 'true' });
      done();
    };

    const mockResponse = createMockResponse(evalExpect);
    articleRouteController.classifyArticle(mockRequest, mockResponse, parameters, 'sanitised');
  });

  test('classifyArticle() controller with valid input with timeout query, which causes a timeout', done => {
    jest.useFakeTimers();
    const mockRequest = {
      query: {
        dbID: 'test-id',
        timeout: '700'
      },
      headers: {}
    };

    const parameters = {
      input: {
        dbID: 'test-id'
      }
    };

    const orchestrationResolved = createMockResponseObject({
      input: {
        dbID: 'test-id',
        cmsID: undefined,
        url: undefined
      }
    });

    const expectedOrchestrationInput = {
      input: {
        dbID: 'test-id',
        cmsID: undefined,
        url: undefined
      }
    };

    mockLoadModule.objectUtils.string.isJson.mockReturnValueOnce(true);
    mockLoadModule.analyzer.orchestrator.analyzeHtml.mockImplementation(() => new Promise(resolve => setTimeout(() => resolve(orchestrationResolved), 2000)));
    let doneCalled = false;
    const evalExpect = (response, mockResponse) => {
      if (!doneCalled) {
        expect(mockLoadModule.objectUtils.buildResponse.init).toBeCalledTimes(0);
        expect(mockResponse.status).toBeCalledWith(200);
        expect(mockLoadModule.analyzer.orchestrator.analyzeHtml).toBeCalledWith(expectedOrchestrationInput, 'testUserId', undefined);
        expect(response).toMatchObject({ error: 'Request has timed out' });
        doneCalled = true;
        done();
      }
    };

    const mockResponse = createMockResponse(evalExpect);
    articleRouteController.classifyArticle(mockRequest, mockResponse, parameters);
    jest.advanceTimersByTime(2000);
  });

  test('classifyArticleFromRequest() controller with no body in request, returns an error', done => {
    const evalExpect = (response, mockResponse) => {
      expect(mockLoadModule.objectUtils.buildResponse.init).toBeCalledTimes(0);
      expect(mockResponse.status).toBeCalledWith(200);
      expect(mockLoadModule.analyzer.orchestrator.analyzeHtml).toBeCalledTimes(0);
      expect(response).toMatchObject({ error: 'the request is lacking body properties' });
      done();
    };
    mockLoadModule.objectUtils.string.isJson.mockReturnValueOnce(true);
    const mockResponse = createMockResponse(evalExpect);
    articleRouteController.classifyArticleFromRequest({ query: {}, headers: {} }, mockResponse, 'queue');
  });

  test('classifyArticleFromRequest() controller with valid input, but orchestrator returns error', done => {
    const request = {
      query: {},
      headers: {},
      body: {
        html: 'some html',
        text: 'some text'
      }
    };
    mockLoadModule.objectUtils.string.isJson.mockReturnValueOnce(true);
    mockLoadModule.analyzer.orchestrator.analyzeHtml.mockResolvedValueOnce({ error: 'article Id not found' });
    mockLoadModule.objectUtils.buildResponse.init.mockReturnValueOnce({ error: 'article Id not found' });
    const expectedOrchestrationInput = {
      input: {
        html: 'some html',
        text: 'some text'
      }
    };

    const evalExpect = (response, mockResponse) => {
      expect(mockLoadModule.objectUtils.buildResponse.init).toBeCalledTimes(1);
      expect(mockLoadModule.objectUtils.buildResponse.init).toBeCalledWith({ error: 'article Id not found' }, undefined, 'queue');
      expect(mockResponse.status).toBeCalledWith(200);
      expect(mockLoadModule.analyzer.orchestrator.analyzeHtml).toBeCalledWith(expectedOrchestrationInput, 'testUserId', undefined);
      expect(response).toMatchObject({ error: 'article Id not found' });
      done();
    };

    const mockResponse = createMockResponse(evalExpect);
    articleRouteController.classifyArticleFromRequest(request, mockResponse, 'queue');
  });

  test('classifyArticleFromRequest() controller with valid input, orchestrator returns findings', done => {
    const request = {
      query: {},
      headers: {},
      body: {
        html: 'some html',
        text: 'some text'
      }
    };

    const orchestrationResolved = createMockResponseObject({
      input: {
        dbID: 'test-id',
        cmsID: undefined,
        url: undefined,
        html: 'some html',
        text: 'some text'
      }
    });

    const expectedOrchestrationInput = {
      input: {
        html: 'some html',
        text: 'some text'
      }
    };

    const expectedResponse = { message: 'article received', status: 'inProgress' };
    mockLoadModule.objectUtils.string.isJson.mockReturnValueOnce(true);
    mockLoadModule.analyzer.orchestrator.analyzeHtml.mockResolvedValueOnce(orchestrationResolved);
    mockLoadModule.objectUtils.buildResponse.init.mockReturnValueOnce(expectedResponse);
    const evalExpect = (response, mockResponse) => {
      expect(mockLoadModule.objectUtils.buildResponse.init).toBeCalledTimes(1);
      expect(mockLoadModule.objectUtils.buildResponse.init).toBeCalledWith(orchestrationResolved, undefined, 'queue');
      expect(mockResponse.status).toBeCalledWith(200);
      expect(mockLoadModule.analyzer.orchestrator.analyzeHtml).toBeCalledWith(expectedOrchestrationInput, 'testUserId', undefined);
      expect(response).toMatchObject(expectedResponse);
      done();
    };

    const mockResponse = createMockResponse(evalExpect);
    articleRouteController.classifyArticleFromRequest(request, mockResponse, 'queue');
  });

  test('classifyArticleRawData() controller with valid input, orchestrator returns findings', done => {
    const request = {
      query: {
        url: 'https://mantis-test.com'
      },
      headers: {},
      body: {}
    };

    const orchestrationResolved = createMockResponseObject({
      input: {
        url: 'https://mantis-test.com'
      }
    });

    const expectedOrchestrationInput = {
      input: {
        url: 'https://mantis-test.com'
      }
    };

    const expectedResponse = {
      text: {},
      images: []
    };
    mockLoadModule.objectUtils.string.isJson.mockReturnValueOnce(true);
    mockLoadModule.analyzer.orchestrator.analyzeHtml.mockResolvedValueOnce(orchestrationResolved);
    mockLoadModule.objectUtils.buildResponse.init.mockReturnValueOnce(expectedResponse);
    const evalExpect = (response, mockResponse) => {
      expect(mockResponse.status).toBeCalledWith(200);
      expect(mockLoadModule.analyzer.orchestrator.analyzeHtml).toBeCalledWith(expectedOrchestrationInput, 'testUserId', undefined);
      expect(response).toMatchObject(expectedResponse);
      done();
    };

    const mockResponse = createMockResponse(evalExpect);
    articleRouteController.classifyArticleRawData(request, mockResponse, 'queue');
  });
});
