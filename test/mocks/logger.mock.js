const mockLogger = {
  Logger: {
    fn: {
      error: jest.fn(),
      warn: jest.fn(),
      http: jest.fn(),
      info: jest.fn(),
      verbose: jest.fn(),
      debug: jest.fn()
    },
    // eslint-disable-next-line object-shorthand
    buildLogger: function() {
      return this.fn;
    },
    // eslint-disable-next-line object-shorthand
    reset: function() {
      this.fn.error.mockReset();
      this.fn.error.mockClear();

      this.fn.warn.mockReset();
      this.fn.warn.mockClear();

      this.fn.http.mockReset();
      this.fn.http.mockClear();

      this.fn.info.mockReset();
      this.fn.info.mockClear();

      this.fn.verbose.mockReset();
      this.fn.verbose.mockClear();

      this.fn.debug.mockReset();
      this.fn.debug.mockClear();
    }
  }
};

module.exports = mockLogger;
