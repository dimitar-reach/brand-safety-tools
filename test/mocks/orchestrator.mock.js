/* eslint-disable object-shorthand */
const orchestrator = {
  analyzeText: jest.fn(),
  analyzeImage: jest.fn(),
  analyzeArticle: jest.fn(),
  analyzeHtml: jest.fn(),
  getDBCachedResult: jest.fn(),
  getCachedArticleFindings: jest.fn(),
  reset: function() {
    this.analyzeText.mockReset();
    this.analyzeText.mockClear();

    this.analyzeImage.mockReset();
    this.analyzeImage.mockClear();

    this.analyzeArticle.mockReset();
    this.analyzeArticle.mockClear();

    this.analyzeHtml.mockReset();
    this.analyzeHtml.mockClear();

    this.getDBCachedResult.mockReset();
    this.getDBCachedResult.mockClear();

    this.getCachedArticleFindings.mockReset();
    this.getCachedArticleFindings.mockClear();
  }
};

module.exports = orchestrator;
/* eslint-enable object-shorthand */
