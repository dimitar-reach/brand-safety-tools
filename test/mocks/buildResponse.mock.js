const responseBuilderMock = {
  init: jest.fn(),
  responseType: {
    raw: 'raw',
    default: 'default',
    queue: 'queue'
  },
  // eslint-disable-next-line object-shorthand
  reset: function() {
    this.init.mockReset();
    this.init.mockClear();
  }
};

module.exports = responseBuilderMock;
