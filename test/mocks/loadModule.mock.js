const log = require('./logger.mock');
const orchestrator = require('./orchestrator.mock');
const buildResponse = require('./buildResponse.mock');
const string = require('./string.mock');
const nlu = jest.fn();

const mockLoadModules = {
  analyzer: {
    orchestrator
  },
  objectUtils: {
    buildResponse,
    string
  },
  log,
  proxies: {
    nlu
  },
  /* eslint-disable object-shorthand */
  reset: function() {
    log.Logger.reset();
    orchestrator.reset();
    buildResponse.reset();
    string.reset();
  }
};

module.exports = mockLoadModules;
