const mockStringUtils = {
  filtersFromString: jest.fn(),
  isJson: jest.fn(),
  // eslint-disable-next-line object-shorthand
  reset: function() {
    this.filtersFromString.mockReset();
    this.filtersFromString.mockClear();

    this.isJson.mockReset();
    this.isJson.mockClear();
  }
};

module.exports = mockStringUtils;
