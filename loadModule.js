const analyzer = process.env.appid === 'local' ? require('./services/analyzer') : require('@trinitymirrordigital/mantis-analyzer-service');

const configuration = process.env.appid === 'local' ? require('./services/configuration') : require('@trinitymirrordigital/mantis-configuration-service');
const ruleManager = process.env.appid === 'local' ? require('./services/rule-management') : require('@trinitymirrordigital/mantis-rule-management-service');
const authentication = process.env.appid === 'local' ? require('./utilities/authentication') : require('@trinitymirrordigital/mantis-authentication-utility');
const log = process.env.appid === 'local' ? require('./utilities/log') : require('@trinitymirrordigital/mantis-log-utility');
const objectUtils = process.env.appid === 'local' ? require('./utilities/object') : require('@trinitymirrordigital/mantis-object-utility');
const proxies = process.env.appid === 'local' ? require('./utilities/proxies') : require('@trinitymirrordigital/mantis-proxies-utility');

module.exports = {
  analyzer,
  configuration,
  ruleManager,
  authentication,
  log,
  objectUtils,
  proxies
};
