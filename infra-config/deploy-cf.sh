#!/bin/bash
# Push app
echo "//registry.npmjs.org/:_authToken=${NPM_TOKEN}" > .npmrc

enable_autoscale () {
  echo ""
  echo ""
  echo "preparing to add auto scalling policy"
  if [[ $AUTOSCALE_POLICY_ENABLED == true ]]; then
    cf install-plugin -r CF-Community app-autoscaler-plugin -f
    cf aasp "$CF_APP" "./infra-config/${MANTIS_STACK}-$CF_SPACE-autoscale-rules.json"
  else
    echo "skipping auto scale"
  fi
}

#if it doesnt find an app, it pushes the app
if ! cf app "$CF_APP"; then  
  cf push "$CF_APP"
  #bind private endpoint
  enable_autoscale
else
  OLD_CF_APP="${CF_APP}-OLD-$(date +"%s")"
  #if crap happens rename old app again back to this one.
  rollback() {
    set +e  
    if cf app "$OLD_CF_APP"; then
      cf logs "$CF_APP" --recent
      cf delete "$CF_APP" -f
      cf rename "$OLD_CF_APP" "$CF_APP"
    fi
    exit 1
  }
  set -e
  trap rollback ERR
  #otherwise rename and delete old one.
  cf rename "$CF_APP" "$OLD_CF_APP"
  cf push "$CF_APP"
  #bind autoscale policy
  enable_autoscale
  #delete old app
  cf delete "$OLD_CF_APP" -f
  
  #this plugin is not include in the toolchain image...
  
fi
# Export app name and URL for use in later Pipeline jobs
export CF_APP_NAME="$CF_APP"
export APP_URL=http://$(cf app $CF_APP_NAME | grep -e urls: -e routes: | awk '{print $2}')
# View logs
#cf logs "${CF_APP}" --recent
