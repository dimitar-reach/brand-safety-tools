CREATE TABLE PARAMS (
	ID SERIAL NOT NULL PRIMARY KEY,
	NAME VARCHAR(256),
	VALUE VARCHAR(256)
);

CREATE OR REPLACE FUNCTION public.notify_params()
    RETURNS trigger
    LANGUAGE  plpgsql
AS $function$
BEGIN
    PERFORM pg_notify('params_event', row_to_json(NEW)::text);
    RETURN NULL;
END;
$function$

CREATE TRIGGER insert_params_trigger AFTER INSERT ON params
    FOR EACH ROW EXECUTE PROCEDURE notify_params();

CREATE TRIGGER update_params_trigger AFTER UPDATE ON params
    FOR EACH ROW EXECUTE PROCEDURE notify_params();
    
CREATE TRIGGER update_params_trigger AFTER DELETE ON params
    FOR EACH ROW EXECUTE PROCEDURE notify_params();