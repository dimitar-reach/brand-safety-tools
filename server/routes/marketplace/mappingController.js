const routeUtils = require('./../routeUtils');
const XLSX = require('xlsx');
const { proxies } = require('../../../loadModule');
const psql = proxies.psql;


const deleteMarketplaceMappingFile = (req, res) => {
    if (!req.params || !req.params.version) {
      next('No version param!');
      return;
    }
    psql
      .deleteMarketplaceFile(req.params.version)
      .then(response => {
        routeUtils.handleSuccess(res, response);
      })
      .catch(err => {
        routeUtils.handleError(res, err);
      });
  }

const getMarketplaceMappingFile = (req, res, next) => {
    if (!req.params || !req.params.version) {
      next('No version param!');
      return;
    }
    psql
      .getMarketplaceSpreadsheet(req.params.version)
      .then(result => {
        if (!result || result.rowCount === 0) {
          next('no spreadsheets available!');
        }
  
        const file = Buffer.from(result.rows[0].xls_file, 'base64');
        res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        // res.setHeader('Content-Disposition', 'attachment; filename=' + filename);
        routeUtils.handleSuccess(res, file, 0);
      })
      .catch(err => {
        next(err);
      });
  }

  /**
   * 
   * @param {*} fileBuffer  buffer
   * @returns a JSON object in the form of {"customers": {}, "segments": {}} to be stored in PARAMS table
   */
const tranformXLSToJSON = (fileBuffer) => {
    const workbook = XLSX.read(fileBuffer);
    const sheet_name_list = workbook.SheetNames;
    const xlsFileListOfJson = XLSX.utils.sheet_to_json(workbook.Sheets[sheet_name_list[0]], { raw: true, defval: null });
  
    const segmentObject = { segments: {} };
    const customerObject = { customers: {} };
    xlsFileListOfJson.forEach(row => {
      if (row['Mantis Customer Name'].startsWith('context_')){
        // note: "Xandr Segment Name" column is soly for use of the editors => do not use that columns data 
        const segmentName = row['Mantis Customer Name'].split("context_")[1];

        if(row['Segment ID']){
          segmentObject['segments'][segmentName] = row['Segment ID'];
        }

      }else{
        if(row['Segment ID']){
          customerObject['customers'][row['Mantis Customer Name']]= row['Segment ID']
        }
      }
    });
    
    let paramObjectToStore = {};
    paramObjectToStore = { ...customerObject, ...segmentObject };
    const paramJSONObjectToStore = JSON.stringify(paramObjectToStore);
    return paramJSONObjectToStore;
}

const transformAndStoreMarketMappingSpreadsheet = (req, res, next) => {
    const appid = process.env.appid;
    paramJSONObjectToStore = tranformXLSToJSON(req.file.buffer);
    psql
      .insertMarketplace(appid, paramJSONObjectToStore, req.file.buffer, null, res.username)
      .then(() => {
        routeUtils.handleSuccess(res, { message: 'saved spreadsheet' }, 0);
      })
      .catch(err => {
        next(err);
      });
  }

  const getAllMarketplaceMappingFiles = (req, res, next) => {
    psql
      .getAllMarketplaceSpreadsheetFiles()
      .then(response => {
        routeUtils.handleSuccess(res, response.rows, true);
      })
      .catch(err => {
        routeUtils.handleError(res, err);
        next(err);
      });
  }

module.exports = {
    deleteMarketplaceMappingFile,
    getMarketplaceMappingFile,
    getAllMarketplaceMappingFiles, 
    transformAndStoreMarketMappingSpreadsheet,
    tranformXLSToJSON
}