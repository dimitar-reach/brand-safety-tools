// error handler
const error = (err, req, res, next) => {
  // log error to console
  if (!err.message) {
    console.error('App error handler: ' + JSON.stringify(err, null, 2));
  } else if (err.noStack) {
    console.error('App error handler: ' + err.message);
  } else {
    console.error('App error handler: ' + err.message + '\r\n\r\n' + err.stack);
  }
  if (err && err.isMantisError) {
    res.status(err.httpStatusCode);
    res.send(err.buildJSONResponseError());
  } else {
    res.status(err.status || 500);
    res.send(err.message || err);
  }
};

const notFound = (req, res, next) => {
  console.error('Not Found...');
  const err = new Error('Not Found...');
  err.status = 404;
  err.noStack = true;
  next(err);
};

module.exports = { error, notFound };
