/*****************************************************************
 *
 * Licensed Materials - Property of IBM
 *
 * (C) Copyright IBM Corp. 2019. All Rights Reserved.
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with IBM Corp.
 *
 *******************************************************************/
/*
 * routeRoot.js
 */
/*------------------------------------------------------------------*/
/*  IMPORT                                                          */
/*------------------------------------------------------------------*/

const express = require('express');
const heapdump = require('heapdump');
const fs = require('fs');
const router = express.Router();
const { configuration, analyzer, ruleManager, proxies, log, authentication } = require('../../loadModule');

const urlExcelReader = analyzer.urlExcelReader;
//let wvr = require("../proxies/wvr");
//let nlu = require("../proxies/nlu");
const articles = proxies.db.articles;
const psql = proxies.psql;
const routeUtils = require('./routeUtils');
const logger = log.Logger.buildLogger(module.filename);

const articleController = require('./article/articleController');
const { type: responseType } = require('../../loadModule').objectUtils.buildResponse;
const {
  getMarketplaceMappingFile,
  getAllMarketplaceMappingFiles,
  deleteMarketplaceMappingFile,
  transformAndStoreMarketMappingSpreadsheet
} = require('./marketplace/mappingController');
/*
 *	MAIN
 */

//Retrieve all images from local directory
router.get('/images', function(req, res) {
  const files = fs.readdirSync('client/public/images');
  res.status(200);
  res.json(files);
});

router.get('/ua', (req, res) => {
  const ua = req.get('user-agent');
  logger.debug(`User Agent: ${ua}`);
  routeUtils.handleSuccess(res, { ua });
});

router.get('/classifyFile/:filename', function(req, res) {
  const filename = 'client/public/images/' + req.params.filename;
  logger.debug(`reading file ${filename}`);

  if (fs.existsSync(filename)) {
    const response = {
      input: {
        file: filename,
        buffer: fs.createReadStream(filename)
      }
    };

    const promise = analyzer.orchestrator.analyzeImage(response);
    promise
      .then(response => {
        routeUtils.handleSuccess(res, response);
      })
      .catch(err => {
        routeUtils.handleError(res, err);
      });
  } else {
    routeUtils.handleError(res, 'requested file does not exist: ' + filename);
  }
});

// Upload an image to VR (input in req.file, see https://www.npmjs.com/package/multer#file-information)
router.post('/classifyImageFile', routeUtils.uploadImage.single('file'), function(req, res) {
  if (req.file) {
    const filename = req.file.originalname;
    logger.debug(`classify image file ${filename}`);

    const response = {
      input: {
        filename,
        mimetype: req.file.mimetype,
        buffer: req.file.buffer
      }
    };

    const promise = analyzer.orchestrator.analyzeImage(response);
    promise
      .then(response => {
        routeUtils.handleSuccess(res, response);
      })
      .catch(err => {
        routeUtils.handleError(res, err);
      });
  } else {
    routeUtils.handleError(res, 'upload failed - make sure you upload a JPEG or PNG image');
  }
});

// analyze the image at the input url and return a full analysis response structure
router.get('/classifyImageURL', function(req, res) {
  const url = req.query.url;
  logger.debug(`classify image by url ${url}`);

  const response = {
    input: { images: [url] }
  };

  const promise = analyzer.orchestrator.analyzeImage(response);
  promise
    .then(response => {
      routeUtils.handleSuccess(res, response);
    })
    .catch(err => {
      routeUtils.handleError(res, err);
    });
});

//Upload a text file to NLU (input in req.file, see https://www.npmjs.com/package/multer#file-information)
router.post('/classifyTextFile', routeUtils.uploadTextHtmlJsonFile.single('file'), function(req, res) {
  if (req.file) {
    const filename = req.file.originalname;
    logger.debug(`classify text file ${filename}`);

    const response = {
      input: {}
    };

    // replace complete input object with json file
    if (req.file.mimetype === 'text/html') {
      response.input.html = req.file.buffer.toString('utf8');
    } else if (req.file.mimetype === 'application/json') {
      response.input = JSON.parse(req.file.buffer.toString('utf8'));
    } else if (req.file.mimetype === 'text/plain') {
      response.input.text = req.file.buffer.toString('utf8');
    }

    response.input.filename = filename;
    response.input.mimetype = req.file.mimetype;

    const promise = analyzer.orchestrator.analyzeText(response, res.userID);
    promise
      .then(response => {
        routeUtils.handleSuccess(res, response);
      })
      .catch(err => {
        routeUtils.handleError(res, err);
      });
  } else {
    routeUtils.handleError(res, 'upload failed - make sure you upload a txt, html or json file');
  }
});

router.get('/classifyText', function(req, res) {
  const url = req.query.url;
  if (!url) {
    logger.debug('url was not passed');
    routeUtils.handleSuccess(res, { error: 'url was not passed' });
    return;
  }
  logger.debug(`classify text by url ${url}`);
  const response = {
    input: { url }
  };

  const promise = analyzer.orchestrator.analyzeText(response, res.userID);
  promise
    .then(response => {
      routeUtils.handleSuccess(res, response);
    })
    .catch(err => {
      routeUtils.handleError(res, err);
    });
});

router.post('/classifyText', function(req, res) {
  if (!req.body || (!req.body.html && !req.body.text)) {
    logger.debug('Body is empty');
    routeUtils.handleSuccess(res, { error: 'Body is empty' });
    return;
  }
  logger.debug(`classify json text ${JSON.stringify(req.body)}`);

  const response = {
    input: req.body
  };

  const promise = analyzer.orchestrator.analyzeText(response, res.userID);
  promise
    .then(response => {
      routeUtils.handleSuccess(res, response);
    })
    .catch(err => {
      routeUtils.handleError(res, err);
    });
});

router.put('/article', (req, res) => articleController.classifyArticleFromRequest(req, res, responseType.queue));
router.get('/article/classification', (req, res) => articleController.getClassification(req, res, false, responseType.sanitised));
router.get('/article/classification/ratings', (req, res) => articleController.getClassification(req, res, true, responseType.sanitised));

/**
 * Analyze article.
 *
 * @param req - parameters: url, images [], title, author, published
 * @param res - structure containing { input, ratings [], findings }
 * @returns
 */

router.get('/classifyArticle', (req, res) => {
  if (process.env.dynamic_paused) {
    logger.error('Paused');
    routeUtils.handleSuccess(res, { status: 'Paused' });
    return;
  }
  if (process.env.dynamic_user_agent_blockers_json) {
    const ua = req.get('user-agent');
    const blocked = JSON.parse(process.env.dynamic_user_agent_blockers_json);
    if (
      blocked.filter(blockedUA => {
        return blockedUA === ua;
      }).length > 0
    ) {
      logger.error('Blocked', { ua });
      routeUtils.handleSuccess(res, { status: 'Blocked! - please contact the mantis team in slack' });
      return;
    }
  }
  const { cmsID, dbID, url, title, author, exactVersion, forceRescan } = req.query;
  const lastModified = req.query.lastModified ? new Date(req.query.lastModified).toISOString() : undefined;
  const published = req.query.published ? new Date(req.query.published).toISOString() : undefined;

  if (process.env.dynamic_query_blockers) {
    // it's json
    const blockedParams = JSON.parse(process.env.dynamic_query_blockers);
    if (
      blockedParams.filter(paramObj => {
        return req.query[paramObj.name] && req.query[paramObj.name] === paramObj.value;
      }).length > 0
    ) {
      logger.error('Blocked', { blockedParams });
      routeUtils.handleSuccess(res, { status: 'Blocked! - please contact the mantis team in slack' });
      return;
    }
  }
  if (process.env.dynamic_block_cms_ids && req.query.cmsID) {
    const blocked = JSON.parse(process.env.dynamic_block_cms_ids);
    const cmsID = req.query.cmsID;
    if (
      blocked.filter(blockedId => {
        return cmsID.indexOf(blockedId) > -1;
      }).length > 0
    ) {
      logger.error('Blocked by cms ID');
      routeUtils.handleSuccess(res, { status: 'Blocked' });
      return;
    }
  }
  let images = req.query.images || [];
  if (!url && !cmsID && images.length === 0 && !dbID) {
    logger.error('a required param (url, cmsID, dbID or images) was not passed');
    routeUtils.handleSuccess(res, { error: 'a required param (url, cmsID, dbID or images) was not passed' });
    return;
  }

  // if images has been added as argument in URL, but with empty value, force it to be an empty array
  if (typeof images === 'string') {
    images = [images];
  }
  const parameters = {
    input: { filter: (req.query.filter || '').split(','), cmsID, dbID, url, images, title, author, published, lastModified, exactVersion, forceRescan }
  };
  articleController.classifyArticle(req, res, parameters, responseType.raw);
});

/**
 * Return raw NLU data from an article
 */
router.get('/articleRawData', async (req, res) => {
  articleController.classifyArticleRawData(req, res);
});

/**
 * Analyze article.
 *
 * @param req body - json containing { url, images [], title, author, published }
 * @param res - structure containing { input, ratings [], findings }
 * @returns
 */
router.post('/classifyArticle', (req, res) => {
  if (!req.body || (!req.body.html && !req.body.text)) {
    logger.error('the request is lacking body properties');
    routeUtils.handleSuccess(res, { error: 'the request is lacking body properties' });
    return;
  }
  const parameters = {
    input: req.body
  };
  if (req.body.lastModified) {
    parameters.input.lastModified = new Date(req.body.lastModified).toISOString();
  }
  if (req.body.published) {
    parameters.input.published = new Date(req.body.published).toISOString();
  }
  articleController.classifyArticle(req, res, parameters, responseType.raw);
});

router.post('/processExcel', routeUtils.uploadAnyFile.single('file'), function(req, res) {
  logger.debug(`process excel file ${req.file}`);

  if (req.file) {
    const json = urlExcelReader.parseExcel(req.file.buffer);

    routeUtils.handleSuccess(res, json);
  } else {
    routeUtils.handleError('No Excel file with URLs uploaded');
  }
});

router.post('/feedback', function(req, res) {
  const analysisResponse = req.body;
  logger.debug('store feedback');

  const promise = articles.storeArticle(analysisResponse, res.userID);
  promise
    .then(response => {
      routeUtils.handleSuccess(res, response);
    })
    .catch(err => {
      routeUtils.handleError(res, err);
    });
});

router.get('/ratings', function(req, res) {
  logger.debug('get ratings');

  psql
    .getRatings()
    .then(response => {
      routeUtils.handleSuccess(res, response);
    })
    .catch(err => {
      routeUtils.handleError(res, err);
    });
});

router.get('/getRatingsConfusionMatrix', function(req, res) {
  logger.debug('get ratings confusion matrix');

  psql
    .getRatingsConfusionMatrix()
    .then(response => {
      routeUtils.handleSuccess(res, response);
    })
    .catch(err => {
      routeUtils.handleError(res, err);
    });
});

router.get('/getFindingsAsTable', function(req, res) {
  logger.debug('getFindingsAsTable');

  psql
    .getFindingsAsTable()
    .then(response => {
      routeUtils.handleSuccess(res, response);
    })
    .catch(err => {
      routeUtils.handleError(res, err);
    });
});

router.post('/setParams', function(req, res) {
  const { id, value, type } = req.body;
  if (configuration.validate(type, value).errors.length !== 0) {
    const err = { message: 'Validation failed, the value entered is not properly formatted' };
    routeUtils.handleError(res, err);
    return;
  }
  psql
    .updateParam([id, value])
    .then(response => {
      routeUtils.handleSuccess(res, response);
    })
    .catch(err => {
      routeUtils.handleError(res, err);
    });
});

router.post('/insertParams', function(req, res) {
  const { name, value, appid, type } = req.body;
  if (configuration.validate(type, value).errors.length !== 0) {
    const err = { message: 'Validation failed, check if you selected the correct type' };
    routeUtils.handleError(res, err);
    return;
  }
  psql
    .insertParam([name, value, appid, type])
    .then(response => {
      routeUtils.handleSuccess(res, response);
    })
    .catch(err => {
      routeUtils.handleError(res, err);
    });
});

router.post('/marketplaceMapping/delete/file/version/:version', deleteMarketplaceMappingFile);

router.get('/marketplaceMapping/file/version/:version', getMarketplaceMappingFile);

router.post('/marketplaceMapping/file/', routeUtils.uploadExcelFile.single('file'), transformAndStoreMarketMappingSpreadsheet);

router.get('/marketplaceMapping/files/', getAllMarketplaceMappingFiles);

router.post('/getParams', function(req, res) {
  psql
    .getParams()
    .then(response => {
      routeUtils.handleSuccess(res, response);
    })
    .catch(err => {
      routeUtils.handleError(res, err);
    });
});

router.post('/deleteParam', function(req, res) {
  psql
    .deleteParam(req.body.id)
    .then(response => {
      routeUtils.handleSuccess(res, response);
    })
    .catch(err => {
      routeUtils.handleError(res, err);
    });
});

configuration.setRoutes(router, routeUtils);
authentication.setRoutes(router, routeUtils);
analyzer.setRoutes(router, routeUtils.setRequestTimeout);
ruleManager.setRoutes(router);

module.exports = router;
