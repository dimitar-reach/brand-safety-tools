function getFeaturesList() {
  return Buffer.from(process.env.dynamic_feature_flags ? process.env.dynamic_feature_flags : '{}').toString('base64');
}

const setCookies = (req, res, next) => {
  // sets features list as json stringified in a base64 string
  res.cookie('configuration_service', process.env.configuration_service, { maxAge: 900000, encode: String });
  res.cookie('clfeat', getFeaturesList(), { maxAge: 900000, encode: String });
  next();
};

module.exports = setCookies;
