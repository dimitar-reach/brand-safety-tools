const modules = require('../../../loadModule');
const logger = modules.log.Logger.buildLogger(module.filename);
const routeUtils = require('../routeUtils');
const { orchestrator } = modules.analyzer;
const { buildResponse } = modules.objectUtils;
const { filtersFromString } = modules.objectUtils.string;
const { nlu } = modules.proxies;

const getRequestFilters = req => {
  let filter = req.query.filter ? req.query.filter.split(',') : undefined;
  if (req.query.ratingsOnly === 'true') {
    filter = !filter ? ['ratings'] : filter.indexOf('ratings') === -1 ? [...filter, 'ratings'] : filter;
  }
  return filter;
};

const getRequestParameters = req => {
  const { cmsID, dbID, url, canRecalculate, exactVersion } = req.query;
  if (!cmsID && !dbID && !url) {
    return undefined;
  }
  const lastModified = req.query.lastModified ? new Date(req.query.lastModified).toISOString() : undefined;
  return { input: { cmsID, dbID, url, canRecalculate, lastModified, exactVersion, filter: getRequestFilters(req) } };
};

const sendResponse = (res, response, filter, requestMethod, responseType) => {
  if (res.headersSent) return;
  if (requestMethod === 'GET' && process.env.dynamic_header_last_modified && response.lastModified) {
    res.set('Last-Modified', response.lastModified);
  }
  const builtResponse = buildResponse.init(response, filter, responseType);
  if (builtResponse.lastModified) {
    builtResponse.lastModified = new Date(builtResponse.lastModified).toISOString();
  }
  if (builtResponse.published) {
    builtResponse.published = new Date(builtResponse.published).toISOString();
  }
  routeUtils.handleSuccess(res, builtResponse);
};

const getClassification = (req, res, ratingsOnly, responseType) => {
  if (process.env.dynamic_paused) {
    logger.error('Paused');
    routeUtils.handleSuccess(res, { status: 'Paused' });
    return;
  }
  if (process.env.dynamic_user_agent_blockers_json) {
    const ua = req.get('user-agent');
    const blocked = JSON.parse(process.env.dynamic_user_agent_blockers_json);
    if (
      blocked.filter(blockedUA => {
        return blockedUA === ua;
      }).length > 0
    ) {
      logger.error('Blocked', { ua });
      routeUtils.handleSuccess(res, { status: 'Blocked! - please contact the mantis team in slack' });
      return;
    }
  }
  if (process.env.dynamic_query_blockers) {
    // it's json
    const blockedParams = JSON.parse(process.env.dynamic_query_blockers);
    if (
      blockedParams.filter(paramObj => {
        return req.query[paramObj.name] && req.query[paramObj.name] === paramObj.value;
      }).length > 0
    ) {
      logger.error('Blocked', { blockedParams });
      routeUtils.handleSuccess(res, { status: 'Blocked! - please contact the mantis team in slack' });
      return;
    }
  }
  if (process.env.dynamic_block_cms_ids) {
    const blocked = JSON.parse(process.env.dynamic_block_cms_ids);
    const cmsID = req.query.cmsID;
    if (
      blocked.filter(blockedId => {
        return cmsID.indexOf(blockedId) > -1;
      }).length > 0
    ) {
      logger.error('Blocked by cms ID');
      routeUtils.handleSuccess(res, { status: 'Blocked' });
      return;
    }
  }
  const parameters = getRequestParameters(req);
  if (!parameters) {
    routeUtils.handleSuccess(res, { error: 'a required param (url, cmsID, dbID) was not passed' });
    return;
  }

  // ratingsOnly used in route for article/classification/ratings
  const filter = ratingsOnly ? ['ratings'] : getRequestFilters(req);
  if (req.query.timeout) {
    routeUtils.setRequestTimeout(req, res);
  }
  orchestrator
    .getCachedArticleFindings(parameters, res.userID)
    .then(result => sendResponse(res, result, filter, req.method, responseType))
    .catch(err => {
      if (res.headersSent) return;
      routeUtils.handleError(res, err);
    });
};

const classifyArticle = (req, res, parameters, responseType) => {
  if (req.query.timeout) {
    routeUtils.setRequestTimeout(req, res);
  }
  const filter = getRequestFilters(req);
  orchestrator
    .analyzeHtml(parameters, res.userID, req.method)
    .then(result => {
      if (req.method === 'POST' && process.env.dynamic_send_to_recommender) {
        const recommenderFilters = ['findings', 'ratings', 'categories', 'sentiment', 'emotion'];
        const builtResponseForStream = buildResponse.init(result, recommenderFilters, 'raw');

        if (builtResponseForStream.findings && Object.keys(builtResponseForStream.findings).length > 0) {
          // Note: only push to "recommender" if findings is not empty
          // add url and cmsID & send to kafka recommender topic
          builtResponseForStream.url = parameters.input.url;
          builtResponseForStream.cmsID = parameters.input.cmsID;
          builtResponseForStream.lastModified = parameters.input.lastModified;
          builtResponseForStream.publishedDate = parameters.input.published;
          builtResponseForStream.customMetaData = parameters.input.customMetaData;
          builtResponseForStream.activateDate = parameters.input.activateDate;
          builtResponseForStream.live = parameters.input.live;
          builtResponseForStream.breakingNews = parameters.input.breakingNews;
          builtResponseForStream.expireDate = parameters.input.expireDate;
          orchestrator.matchAndSendToRecommenderQueue(builtResponseForStream);
        }
      }

      if (req.method === 'POST') {
        orchestrator.sendToQueueETL(result);
      }

      sendResponse(res, result, filter, req.method, responseType);
    })
    .catch(err => {
      if (res.headersSent) return;
      routeUtils.handleError(res, err);
    });
};

const classifyArticleRawData = (req, res) => {
  return new Promise(resolve => {
    if (req.query.timeout) {
      routeUtils.setRequestTimeout(req, res);
    }
    const { url } = req.query;
    orchestrator
      .analyzeHtml({ input: { url } }, res.userID, req.method)
      .then(result => {
        routeUtils.handleSuccess(res, result.findings);
        resolve();
      })
      .catch(err => {
        if (res.headersSent) return;
        routeUtils.handleError(res, err);
      });
  });
};

const classifyArticleFromRequest = (req, res, responseType) => {
  if (!req.body || (!req.body.html && !req.body.text)) {
    logger.error('the request is lacking body properties');
    routeUtils.handleSuccess(res, { error: 'the request is lacking body properties' });
    return;
  }
  const input = req.body;
  const parameters = {
    input
  };
  classifyArticle(req, res, parameters, responseType);
};

module.exports = {
  getClassification,
  classifyArticle,
  classifyArticleRawData,
  classifyArticleFromRequest
};
