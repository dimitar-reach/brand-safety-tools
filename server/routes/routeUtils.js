/*****************************************************************
 *
 * Licensed Materials - Property of IBM
 *
 * (C) Copyright IBM Corp. 2019. All Rights Reserved.
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with IBM Corp.
 *
 *******************************************************************/
/*
 * routeUtils.js
 */
/*------------------------------------------------------------------*/
/*  IMPORT                                                          */
/*------------------------------------------------------------------*/

const multer = require('multer');
const modules = require('../../loadModule');

const stringUtils = modules.objectUtils.string;
const logger = modules.log.Logger.buildLogger(module.filename);

const maxAge = () => process.env.dynamic_header_max_age || 600;
const articleIdNotFoundMaxAge = () => process.env.dynamic_header_article_id_max_age || 300;

function handleSuccess(res, response, bustCache = false) {
  res.status(200);
  if (!response.error) {
    const maxCacheAge = bustCache ? 0 : maxAge();
    res.set('Cache-Control', `public, max-age=${maxCacheAge}`);
  } else if (response.error === 'Article id not found') {
    // attempts to improve cdn caching as sometimes articles will not exist in our db.
    res.set('Cache-Control', `public, max-age=${articleIdNotFoundMaxAge()}`);
  } else {
    res.set('Cache-Control', 'no-store');
  }
  res.send(response);
}

function handleError(res, error) {
  logger.error('handleError(' + error + ')\n' + error.stack);
  res.status(error.status || 500);
  res.set('Cache-Control', 'no-store');
  res.send(makeErrorResponse(error));
}

function makeErrorResponse(error) {
  let message = error;
  if (error.message) {
    message = error.message;
  }
  if (!stringUtils.isJson(message)) {
    message = { error: message };
  }
  return message;
}

const uploadAnyFile = multer();

const uploadTextHtmlJsonFile = multer({
  storage: multer.memoryStorage(),
  fileFilter(req, file, callback) {
    logger.debug(`multer fileFilter: file.mimetype ${file.mimetype}`);

    if (!file) {
      callback(null, false);
    } else if (file.mimetype === 'text/plain') {
      callback(null, true);
    } else if (file.mimetype === 'text/html') {
      callback(null, true);
    } else if (file.mimetype === 'application/json') {
      callback(null, true);
    } else {
      logger.warn(`UPLOAD ERROR: wrong mimetype ${file.mimetype}: allowed text/plain, text/html and application/json`);
      callback(null, false);
    }
  }
});

const uploadImage = multer({
  storage: multer.memoryStorage(),
  fileFilter(req, file, callback) {
    logger.debug(`multer fileFilter: file.mimetype ${file.mimetype}`);

    if (!file) {
      callback(null, false);
    } else if (file.mimetype === 'image/jpeg') {
      callback(null, true);
    } else if (file.mimetype === 'image/png') {
      callback(null, true);
    } else {
      logger.warn('UPLOAD ERROR: wrong mimetype');
      callback(null, false);
    }
  }
});

const uploadExcelFile = multer({
  storage: multer.memoryStorage(),
  fileFilter: (req, file, callback) => {
    logger.debug(`multer fileFilter: file.mimetype ${file.mimetype}`);

    if (!file) {
      callback(null, false);
    } else if (file.mimetype === 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
      callback(null, true);
    } else if (file.mimetype === 'application/vnd.ms-excel') {
      callback(null, true);
    } else {
      logger.warn('UPLOAD ERROR: wrong mimetype: expecting Microsoft Excel files');
      callback(null, false);
    }
  }
});

const uploadNewExcelFile = multer({
  storage: multer.memoryStorage(),
  fileFilter: (req, file, callback) => {
    logger.debug(`multer fileFilter: file.mimetype ${file.mimetype}`);

    if (!file) {
      callback(null, false);
    } else if (file.mimetype === 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
      callback(null, true);
    } else {
      logger.warn('UPLOAD ERROR: wrong mimetype: expecting only new XLSX Microsoft Excel files');
      return callback({ error: 'UPLOAD ERROR: wrong mimetype: expecting only new XLSX Microsoft Excel files' });
    }
  }
});

const setRequestTimeout = (req, res) => {
  if (res.headersSent) return;
  setTimeout(() => {
    if (res.headersSent) return;
    const object = {
      timeout: req.query.timeout,
      url: req.query ? req.query.url : req.body ? req.body.url : null,
      cmsID: req.query ? req.query.cmsID : req.body ? req.body.cmsID : null,
      dbID: req.query ? req.query.dbID : req.body ? req.body.dbID : null
    };
    logger.error(`Request has timed out`, object);
    handleSuccess(res, { error: 'Request has timed out' });
  }, parseInt(req.query.timeout, 10));
};

module.exports = {
  handleSuccess,
  handleError,
  makeErrorResponse,
  uploadAnyFile,
  uploadTextHtmlJsonFile,
  uploadImage,
  uploadExcelFile,
  uploadNewExcelFile,
  setRequestTimeout
};
