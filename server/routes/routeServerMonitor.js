const express = require('express');
const heapdump = require('heapdump');
const { log, proxies } = require('../../loadModule');
const { psql } = proxies;
const logger = log.Logger.buildLogger(module.filename);

const router = express.Router();

// router to allow to monitor the instance with public access
router.get('/health', (req, res) => {
  if (process.env.dynamic_paused_health) {
    logger.error('Paused');
    res.status(200).send({ status: 'UP' });
    return;
  }
  psql
    .healthCheck()
    .then(() => {
      res.status(200).send({ status: 'UP' });
    })
    .catch(error => {
      logger.error(`failed db error check with error ${JSON.stringify(error)}`);
      res.status(500).send({ status: 'down' });
    });
});

router.get('/memoryUsage', (req, res) => {
  const used = process.memoryUsage();
  const response = {};
  for (const key in used) {
    if (used.hasOwnProperty(key)) {
      response[key] = `${Math.round((used[key] / 1024 / 1024) * 100) / 100} MB`;
    }
  }
  const body = response;

  res.send(200, body);
});

router.post('/heapdump', (req, res) => {
  heapdump.writeSnapshot(`heapDump-${Date.now()}.heapsnapshot`, (error, filename) => {
    logger.debug('Heap dump of a bloated server written to', filename);
    if (error) {
      res.status(404).send({ message: 'Failed to save heapdump', status: 'failed', filename });
    } else {
      res.status(200).send({ message: 'saved heapdump', status: 'complete', filename });
    }
  });
});

module.exports = router;
