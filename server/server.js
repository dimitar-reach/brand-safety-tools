// load VCAP env vars from cloud foundry
const cfenv = require('cfenv');
const path = require('path');

const { log, objectUtils, proxies, ruleManager, configuration } = require('../loadModule');

const { getHttpContext, setRequestUUID, logRequest } = objectUtils.requestUtils;
const loggerConfig = log.loggerConfig.init(__dirname);
const {
  processMonitor,
  psql,
  nlu,
  db: { categoriesRuleset }
} = proxies;
const { ratingsRulesetManager } = ruleManager;

let app;

const appEnv = cfenv.getAppEnv();

const init = async express => {
  app = express();
  // view engine setup
  app.set('views', path.join(__dirname, '../client/views'));
  app.set('view engine', 'ejs');
  if (process.env.dynamic_header_etag) {
    app.set('etag', process.env.dynamic_header_etag);
  }
  // security
  app.disable('x-powered-by');
  // Enable reverse proxy support in Express. This causes the
  // the "X-Forwarded-Proto" header field to be trusted so its
  // value can be used to determine the protocol. See
  // http://Expressjs.com/api#app-settings for more details.

  app.enable('trust proxy');

  loggerConfig.update();
  await psql.init();
  await psql.loadDynamicParams();
  if (process.env.configuration_service !== 'true') {
    if (process.env.dynamic_configuration_service_connected) {
      await configuration.getConfig();
    }
    nlu.init();
    await categoriesRuleset.setup();
    await ratingsRulesetManager.setup();
  }
  processMonitor.setup([psql.end]);
  processMonitor.start();
};

const setMiddleware = middleware => {
  return async () => {
    middleware.forEach(value => app.use(...value));
  };
};

const start = async () => {
  // initialise all middlewares (preserving the order in which they were in the app.js)

  const logger = log.Logger.buildLogger(module.filename);
  return new Promise((resolve, reject) => {
    const port = appEnv.port || 3000;
    const address = '0.0.0.0';
    app.listen(port, address, () => {
      logger.info(`started instance: ${address}:${port}, in url: ${appEnv.url}`);
      resolve({ port, address });
    });
  });
};

module.exports = {
  instance: app,
  setMiddleware,
  init,
  start
};
