require('appmetrics-dash').attach();

//load environment params from a file named .env (values found in process.env)
const envFile = process.env.env_name ? '.env.' + process.env.env_name : '.env';
require('dotenv').config({ path: envFile });

const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const favicon = require('serve-favicon');
const path = require('path');

const { authentication, objectUtils } = require('./loadModule');

const { getHttpContext, setRequestUUID, logRequest } = objectUtils.requestUtils;

const setCookies = require('./server/routes/setCookies');
const routeRoot = require('./server/routes/routeRoot');
const instanceRouteTools = require('./server/routes/routeServerMonitor');
const errorRoute = require('./server/routes/error');

const server = require('./server/server');

// Force use of HTTPS, allowing HTTP only when running on localhost
const forceHttps = (req, res, next) => {
  if (req.secure || req.hostname == 'localhost') {
    // request was via https, so do no special handling
    next();
  } else {
    // request was via http, so redirect to https
    res.redirect('https://' + req.headers.host + req.url);
  }
};

/**
 * express middlewares that existed in the app.js using the same order.
 */
const middlewareList = [
  [bodyParser.json({ limit: '50mb' })],
  [bodyParser.urlencoded({ limit: '50mb', extended: false })],
  [getHttpContext()],
  [cors()],
  [setRequestUUID],
  [logRequest()],
  ['/', instanceRouteTools],
  [authentication.basicAuth.process()],
  [setCookies],
  [favicon(path.join(__dirname, './client/public', 'favicon.ico'))],
  [express.static(path.join(__dirname, './client/public'))],
  [forceHttps],
  ['/', routeRoot],
  [errorRoute.notFound],
  [errorRoute.error]
];

server
  .init(express)
  .then(server.setMiddleware(middlewareList))
  .then(server.start)
  .catch(error => {
    console.error('server failed with error:', error);
  });
